# Ambiente de desenvolvimento utilizado:
- Sistema Operacional Windows 10 v2004
- Visual Studio Community 2019 16.7.6.
- Tanto a API quanto a aplicação para testes de integração foram desenvolvidas usando .NET Core 3.1.
- O servidor de banco de dados usado foi o SQL Server 2019 Developer.

# Detalhes da solução desenvolvida:
- Por padrão a API vai rodar na url http://localhost:5000.
- A solução de testes de integração (INTEGRATION_TEST) aponta por padrão para http://localhost:5000.
- Caso a url da aplicação seja alterada, alterar também no appsettings.json do projeto de integração para reflitir a nova url.
- Tanto o projeto Presentation quanto o projeto ApplicationTest da API possuem um appsettings.json com a ConnectionString para o BD.
- Para que os testes possam ser executados com sucesso é necessário que o projeto ApplicationTest aponte para um banco previamente criado.
- Para criar o banco é necessário executar o projeto Presentation ao menos uma vez para que as Migrations de criação de banco, tabelas e seed sejam executadas.
- Após a execução das Migrations o sistema possuíra 2 usuários pré-cadastrados:
  - Login: usuarioadmin - senha: SenhaSecreta - perfil: ADMINISTRADOR
  - Login: usuariocomum - senha: SenhaSecreta2 - perfil: USUARIO

# Desafio Pessoa Desenvolvedora .NET

## 🏗 O que fazer?

- Você deve realizar um *fork* deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, **NÃO** é necessário criar um *Pull Request* para isso, nós iremos avaliar e retornar por e-mail o resultado do teste.

# 🚨 Requisitos

- A API deve ser construída em .NET Core (prioritariamente) ou .NET Framework
- Implementar autenticação e deverá seguir o padrão ***JWT***, lembrando que o token a ser recebido deverá ser no formato ***Bearer***
- Implementar operações no banco de dados utilizando um ***ORM*** ou ***Micro ORM***
- **ORM's/Micro ORM's permitidos:**
    - *Entity Framework Core*
    - *Dapper*
- **Bancos relacionais permitidos**
    - *SQL Server* (prioritariamente)
    - *MySQL*
    - *PostgreSQL*
- As entidades da sua API deverão ser criadas utilizando ***Code First***. Portanto, as ***Migrations*** para geração das tabelas também deverá ser enviada no teste.
- Sua API deverá seguir os padrões REST na construção das rotas e retornos
- Sua API deverá conter documentação viva utilizando ***Swagger***
- Caso haja alguma particularidade de implementação, instruções para execução do projeto deverão ser enviadas

# 🎁 Extra

Estes itens não são obrigatórios, porém desejados.

- Testes unitários
- Teste de integração da API em linguagem de sua preferência (damos importância para pirâmide de testes)
- Cobertura de testes utilizando Sonarqube
- Utilização de *Docker* (enviar todos os arquivos e instruções necessárias para execução do projeto)

# 🕵🏻‍♂️ Itens a serem avaliados

- Estrutura do projeto
- Utilização de código limpo e princípios **SOLID**
- Segurança da API, como autenticação, senhas salvas no banco, *SQL Injection* e outros.
- Boas práticas da Linguagem/Framework
- Seu projeto deverá seguir tudo o que foi exigido na seção  [O que desenvolver?](##--o-que-desenvolver)

# 🖥 O que desenvolver?

Você deverá criar uma API que o site [IMDb](https://www.imdb.com/) irá consultar para exibir seu conteúdo, sua API deverá conter as seguintes funcionalidades:

- Administrador
    - Cadastro
    - Edição
    - Exclusão lógica (desativação)
    - Listagem de usuários não administradores ativos
        - Opção de trazer registros paginados
        - Retornar usuários por ordem alfabética
- Usuário
    - Cadastro
    - Edição
    - Exclusão lógica (desativação)
- Filmes
    - Cadastro (somente um usuário administrador poderá realizar esse cadastro)
    - Voto (a contagem de votos será feita por usuário de 0-4 que indica quanto o usuário gostou do filme)
    - Listagem
        - Opção de filtros por diretor, nome, gênero e/ou atores
        - Opção de trazer registros paginados
        - Retornar a lista ordenada por filmes mais votados e por ordem alfabética
    - Detalhes do filme trazendo todas as informações sobre o filme, inclusive a média dos votos

**Obs.:** 

**Apenas os usuários poderão votar nos filmes e a API deverá validar quem é o usuário que está acessando, ou seja, se é um usuário administrador ou não.**

**Caso não consiga concluir todos os itens propostos, é importante que nos envie a implementação até onde foi possível para que possamos avaliar**