﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataSource.Migrations
{
    public partial class AdicionandoArtistas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Artistas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Artistas", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Artistas",
                columns: new[] { "Id", "Nome" },
                values: new object[,]
                {
                    { 1, "Humphrey Bogart" },
                    { 26, "Laurence Olivier" },
                    { 27, "Ginger Rogers" },
                    { 28, "Gene Kelly" },
                    { 29, "Mae West" },
                    { 30, "Orson Welles" },
                    { 31, "Vivien Leigh" },
                    { 32, "Kirk Douglas" },
                    { 33, "Lillian Gish" },
                    { 34, "James Dean" },
                    { 25, "Grace Kelly" },
                    { 35, "Shirley Temple" },
                    { 37, "Rita Hayworth" },
                    { 38, "Marx Brothers" },
                    { 39, "Lauren Bacall" },
                    { 40, "Buster Keaton" },
                    { 41, "Sophia Loren" },
                    { 42, "Sidney Poitier" },
                    { 43, "Jean Harlow" },
                    { 44, "Robert Mitchum" },
                    { 45, "Carole Lombard" },
                    { 36, "Burt Lancaster" },
                    { 24, "John Wayne" },
                    { 23, "Claudette Colbert" },
                    { 22, "Gregory Peck" },
                    { 2, "Katharine Hepburn" },
                    { 3, "Cary Grant" },
                    { 4, "Bette Davis" },
                    { 5, "James Stewart" },
                    { 6, "Audrey Hepburn" },
                    { 7, "Marlon Brando" },
                    { 8, "Ingrid Bergman" },
                    { 9, "Fred Astaire" },
                    { 10, "Greta Garbo" },
                    { 11, "Henry Fonda" },
                    { 12, "Marilyn Monroe" },
                    { 48, "Clark Gable" },
                    { 13, "Elizabeth Taylor" },
                    { 14, "James Cagney" },
                    { 15, "Judy Garland" },
                    { 16, "Spencer Tracy" },
                    { 17, "Marlene Dietrich" },
                    { 18, "Charlie Chaplin" },
                    { 19, "Joan Crawford" },
                    { 20, "Gary Cooper" },
                    { 21, "Barbara Stanwyck" },
                    { 46, "William Holden" },
                    { 47, "Ava Gardner" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Artistas");
        }
    }
}
