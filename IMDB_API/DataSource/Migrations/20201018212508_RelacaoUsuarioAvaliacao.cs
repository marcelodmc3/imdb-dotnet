﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataSource.Migrations
{
    public partial class RelacaoUsuarioAvaliacao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_AvaliacaoFilmes_UsuarioId",
                table: "AvaliacaoFilmes",
                column: "UsuarioId");

            migrationBuilder.AddForeignKey(
                name: "FK_AvaliacaoFilmes_Users_UsuarioId",
                table: "AvaliacaoFilmes",
                column: "UsuarioId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AvaliacaoFilmes_Users_UsuarioId",
                table: "AvaliacaoFilmes");

            migrationBuilder.DropIndex(
                name: "IX_AvaliacaoFilmes_UsuarioId",
                table: "AvaliacaoFilmes");
        }
    }
}
