﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataSource.Migrations
{
    public partial class RemovendoPerfilMestre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Perfis",
                keyColumn: "Id",
                keyValue: 3);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Perfis",
                columns: new[] { "Id", "Ativo", "Codigo", "Nome" },
                values: new object[] { 3, true, "MASTER", "Master" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Ativo", "Hash", "Login", "Nome", "PerfilId", "Salt" },
                values: new object[] { 3, true, "DnYNvYU/zENgXotmFl1hJxNkGHIM3CD/gPTegHNd74xY37b1EHXdEuUKHf7/EtQhhUuMC7w/WeYEbjG+WPxdAg==", "usuariomestre", "Usuário Master da Silva", 3, "fyO+d8MkrKeHzteS5PB/3XTTOSVQbaw+KHL16h6Jq4k=" });
        }
    }
}
