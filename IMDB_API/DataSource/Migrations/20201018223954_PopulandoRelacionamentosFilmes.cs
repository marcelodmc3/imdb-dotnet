﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataSource.Migrations
{
    public partial class PopulandoRelacionamentosFilmes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "ArtistaFilmes",
                columns: new[] { "FilmeId", "ArtistaId" },
                values: new object[,]
                {
                    { 1, 12 },
                    { 32, 45 },
                    { 33, 6 },
                    { 33, 38 },
                    { 33, 47 },
                    { 33, 36 },
                    { 33, 44 },
                    { 34, 46 },
                    { 34, 15 },
                    { 34, 26 },
                    { 34, 35 },
                    { 34, 25 },
                    { 35, 41 },
                    { 35, 22 },
                    { 35, 32 },
                    { 35, 33 },
                    { 35, 18 },
                    { 36, 1 },
                    { 36, 48 },
                    { 36, 24 },
                    { 36, 31 },
                    { 36, 29 },
                    { 37, 41 },
                    { 37, 23 },
                    { 37, 44 },
                    { 37, 33 },
                    { 37, 3 },
                    { 38, 35 },
                    { 32, 3 },
                    { 32, 35 },
                    { 32, 33 },
                    { 32, 46 },
                    { 26, 36 },
                    { 26, 15 },
                    { 26, 10 },
                    { 27, 12 },
                    { 27, 35 },
                    { 27, 44 },
                    { 27, 23 },
                    { 27, 24 },
                    { 28, 27 },
                    { 28, 15 },
                    { 28, 48 },
                    { 28, 5 },
                    { 28, 22 },
                    { 38, 4 },
                    { 29, 23 },
                    { 29, 12 },
                    { 29, 5 },
                    { 29, 1 },
                    { 30, 31 },
                    { 30, 17 },
                    { 30, 27 },
                    { 30, 43 },
                    { 30, 41 },
                    { 31, 15 },
                    { 31, 27 },
                    { 31, 33 },
                    { 31, 10 },
                    { 31, 19 },
                    { 29, 11 },
                    { 26, 8 },
                    { 38, 26 },
                    { 38, 29 },
                    { 45, 25 },
                    { 45, 23 },
                    { 46, 32 },
                    { 46, 37 },
                    { 46, 24 },
                    { 46, 39 },
                    { 46, 21 },
                    { 47, 8 },
                    { 47, 40 },
                    { 47, 37 },
                    { 47, 23 },
                    { 47, 3 },
                    { 48, 37 },
                    { 48, 3 },
                    { 48, 44 },
                    { 48, 34 },
                    { 48, 27 },
                    { 49, 9 },
                    { 49, 18 },
                    { 49, 42 },
                    { 49, 39 },
                    { 49, 38 },
                    { 50, 37 },
                    { 50, 32 },
                    { 50, 47 },
                    { 50, 9 },
                    { 50, 17 },
                    { 45, 12 },
                    { 45, 31 },
                    { 44, 10 },
                    { 44, 22 },
                    { 39, 45 },
                    { 39, 9 },
                    { 39, 16 },
                    { 39, 34 },
                    { 39, 13 },
                    { 40, 24 },
                    { 40, 10 },
                    { 40, 4 },
                    { 40, 17 },
                    { 40, 39 },
                    { 41, 41 },
                    { 41, 19 },
                    { 41, 48 },
                    { 38, 42 },
                    { 41, 44 },
                    { 42, 19 },
                    { 42, 47 },
                    { 42, 34 },
                    { 42, 11 },
                    { 42, 27 },
                    { 43, 15 },
                    { 43, 26 },
                    { 43, 45 },
                    { 43, 24 },
                    { 43, 40 },
                    { 44, 11 },
                    { 44, 43 },
                    { 44, 28 },
                    { 41, 40 },
                    { 26, 47 },
                    { 45, 17 },
                    { 25, 24 },
                    { 7, 44 },
                    { 7, 11 },
                    { 7, 2 },
                    { 8, 3 },
                    { 8, 43 },
                    { 8, 42 },
                    { 8, 1 },
                    { 8, 21 },
                    { 9, 31 },
                    { 9, 16 },
                    { 9, 15 },
                    { 9, 2 },
                    { 9, 30 },
                    { 10, 42 },
                    { 10, 17 },
                    { 10, 28 },
                    { 10, 4 },
                    { 10, 19 },
                    { 11, 10 },
                    { 11, 11 },
                    { 11, 27 },
                    { 11, 42 },
                    { 12, 32 },
                    { 12, 9 },
                    { 12, 8 },
                    { 12, 5 },
                    { 12, 42 },
                    { 7, 10 },
                    { 7, 16 },
                    { 6, 44 },
                    { 6, 20 },
                    { 25, 7 },
                    { 1, 22 },
                    { 1, 6 },
                    { 1, 11 },
                    { 1, 14 },
                    { 2, 47 },
                    { 2, 26 },
                    { 2, 5 },
                    { 2, 38 },
                    { 2, 27 },
                    { 3, 44 },
                    { 3, 45 },
                    { 3, 36 },
                    { 13, 19 },
                    { 3, 3 },
                    { 4, 10 },
                    { 4, 2 },
                    { 4, 1 },
                    { 4, 36 },
                    { 4, 27 },
                    { 5, 2 },
                    { 5, 44 },
                    { 5, 12 },
                    { 5, 27 },
                    { 5, 6 },
                    { 6, 40 },
                    { 6, 21 },
                    { 6, 12 },
                    { 3, 22 },
                    { 13, 32 },
                    { 11, 8 },
                    { 13, 36 },
                    { 20, 47 },
                    { 20, 35 },
                    { 20, 15 },
                    { 20, 38 },
                    { 20, 2 },
                    { 21, 31 },
                    { 21, 9 },
                    { 21, 37 },
                    { 13, 29 },
                    { 21, 43 },
                    { 22, 14 },
                    { 22, 25 },
                    { 22, 13 },
                    { 19, 46 },
                    { 22, 46 },
                    { 23, 33 },
                    { 23, 11 },
                    { 23, 48 },
                    { 23, 10 },
                    { 23, 3 },
                    { 24, 7 },
                    { 24, 21 },
                    { 24, 36 },
                    { 24, 6 },
                    { 24, 41 },
                    { 25, 12 },
                    { 25, 11 },
                    { 25, 5 },
                    { 22, 7 },
                    { 19, 37 },
                    { 21, 11 },
                    { 19, 42 },
                    { 13, 40 },
                    { 14, 41 },
                    { 14, 35 },
                    { 19, 17 },
                    { 14, 15 },
                    { 14, 9 },
                    { 15, 48 },
                    { 15, 15 },
                    { 15, 45 },
                    { 15, 34 },
                    { 15, 14 },
                    { 16, 45 },
                    { 16, 26 },
                    { 16, 25 },
                    { 14, 20 },
                    { 16, 5 },
                    { 16, 46 },
                    { 18, 11 },
                    { 18, 9 },
                    { 18, 24 },
                    { 18, 48 },
                    { 18, 35 },
                    { 19, 33 },
                    { 17, 8 },
                    { 17, 25 },
                    { 17, 16 },
                    { 17, 32 },
                    { 17, 48 }
                });

            migrationBuilder.InsertData(
                table: "DiretorFilmes",
                columns: new[] { "FilmeId", "DiretorId" },
                values: new object[,]
                {
                    { 28, 27 },
                    { 37, 22 },
                    { 30, 25 },
                    { 31, 25 },
                    { 32, 8 },
                    { 33, 12 },
                    { 34, 14 },
                    { 35, 4 },
                    { 36, 22 },
                    { 29, 30 },
                    { 44, 9 },
                    { 40, 13 },
                    { 41, 23 },
                    { 42, 3 },
                    { 43, 3 },
                    { 45, 17 },
                    { 46, 29 },
                    { 47, 12 },
                    { 48, 1 },
                    { 49, 10 },
                    { 50, 23 },
                    { 27, 17 },
                    { 39, 10 },
                    { 26, 19 },
                    { 38, 22 },
                    { 24, 27 },
                    { 25, 5 },
                    { 1, 20 },
                    { 2, 23 },
                    { 3, 23 },
                    { 5, 14 },
                    { 6, 20 },
                    { 7, 6 },
                    { 8, 19 },
                    { 9, 15 },
                    { 10, 27 },
                    { 11, 25 },
                    { 4, 11 },
                    { 13, 15 },
                    { 12, 13 },
                    { 22, 19 },
                    { 21, 23 },
                    { 23, 14 },
                    { 19, 26 },
                    { 20, 21 },
                    { 17, 24 },
                    { 16, 1 },
                    { 15, 14 },
                    { 14, 24 },
                    { 18, 14 }
                });

            migrationBuilder.InsertData(
                table: "GeneroFilmes",
                columns: new[] { "FilmeId", "GeneroId" },
                values: new object[,]
                {
                    { 32, 13 },
                    { 33, 9 },
                    { 33, 7 },
                    { 33, 4 },
                    { 34, 4 },
                    { 34, 14 },
                    { 36, 7 },
                    { 35, 6 },
                    { 35, 13 },
                    { 35, 11 },
                    { 36, 12 },
                    { 36, 3 },
                    { 37, 4 },
                    { 32, 2 },
                    { 34, 17 },
                    { 32, 12 },
                    { 28, 14 },
                    { 31, 16 },
                    { 26, 6 },
                    { 26, 4 },
                    { 27, 15 },
                    { 27, 3 },
                    { 27, 17 },
                    { 37, 14 },
                    { 28, 2 },
                    { 28, 16 },
                    { 29, 1 },
                    { 29, 3 },
                    { 29, 17 },
                    { 30, 11 },
                    { 30, 5 },
                    { 30, 6 },
                    { 31, 9 },
                    { 31, 13 },
                    { 37, 8 },
                    { 41, 13 },
                    { 38, 10 },
                    { 45, 4 },
                    { 45, 5 },
                    { 46, 9 },
                    { 46, 6 },
                    { 46, 4 },
                    { 47, 2 },
                    { 47, 13 },
                    { 47, 6 },
                    { 48, 16 },
                    { 48, 9 },
                    { 48, 3 },
                    { 49, 12 },
                    { 49, 13 },
                    { 49, 2 },
                    { 50, 14 },
                    { 45, 14 },
                    { 44, 7 },
                    { 44, 11 },
                    { 44, 1 },
                    { 38, 11 },
                    { 39, 14 },
                    { 39, 11 },
                    { 39, 10 },
                    { 40, 3 },
                    { 40, 17 },
                    { 40, 2 },
                    { 38, 13 },
                    { 41, 3 },
                    { 26, 17 },
                    { 42, 8 },
                    { 42, 14 },
                    { 42, 4 },
                    { 43, 7 },
                    { 43, 8 },
                    { 43, 17 },
                    { 41, 1 },
                    { 25, 2 },
                    { 21, 13 },
                    { 25, 9 },
                    { 7, 6 },
                    { 7, 1 },
                    { 7, 3 },
                    { 8, 8 },
                    { 8, 17 },
                    { 8, 12 },
                    { 9, 10 },
                    { 9, 3 },
                    { 9, 6 },
                    { 10, 4 },
                    { 10, 9 },
                    { 10, 10 },
                    { 11, 5 },
                    { 11, 16 },
                    { 11, 6 },
                    { 6, 13 },
                    { 12, 16 },
                    { 6, 12 },
                    { 5, 10 },
                    { 50, 3 },
                    { 1, 16 },
                    { 1, 9 },
                    { 1, 6 },
                    { 2, 2 },
                    { 2, 11 },
                    { 2, 1 },
                    { 3, 6 },
                    { 3, 15 },
                    { 3, 4 },
                    { 4, 15 },
                    { 4, 2 },
                    { 4, 5 },
                    { 5, 11 },
                    { 5, 2 },
                    { 6, 1 },
                    { 25, 5 },
                    { 12, 6 },
                    { 13, 2 },
                    { 19, 1 },
                    { 20, 11 },
                    { 20, 10 },
                    { 20, 13 },
                    { 21, 7 },
                    { 21, 12 },
                    { 22, 11 },
                    { 22, 13 },
                    { 22, 3 },
                    { 23, 10 },
                    { 23, 6 },
                    { 23, 8 },
                    { 24, 10 },
                    { 24, 11 },
                    { 24, 3 },
                    { 19, 10 },
                    { 12, 15 },
                    { 19, 3 },
                    { 18, 3 },
                    { 13, 7 },
                    { 13, 15 },
                    { 14, 9 },
                    { 14, 6 },
                    { 14, 8 },
                    { 15, 8 },
                    { 15, 4 },
                    { 15, 17 },
                    { 16, 17 },
                    { 16, 7 },
                    { 16, 10 },
                    { 17, 8 },
                    { 17, 15 },
                    { 17, 7 },
                    { 18, 16 },
                    { 18, 9 },
                    { 50, 15 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 1, 6 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 1, 11 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 1, 12 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 1, 14 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 1, 22 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 2, 5 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 2, 26 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 2, 27 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 2, 38 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 2, 47 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 3, 3 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 3, 22 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 3, 36 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 3, 44 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 3, 45 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 4, 1 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 4, 2 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 4, 10 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 4, 27 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 4, 36 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 5, 2 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 5, 6 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 5, 12 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 5, 27 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 5, 44 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 6, 12 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 6, 20 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 6, 21 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 6, 40 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 6, 44 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 7, 2 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 7, 10 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 7, 11 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 7, 16 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 7, 44 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 8, 1 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 8, 3 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 8, 21 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 8, 42 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 8, 43 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 9, 2 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 9, 15 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 9, 16 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 9, 30 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 9, 31 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 10, 4 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 10, 17 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 10, 19 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 10, 28 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 10, 42 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 11, 8 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 11, 10 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 11, 11 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 11, 27 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 11, 42 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 12, 5 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 12, 8 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 12, 9 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 12, 32 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 12, 42 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 13, 19 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 13, 29 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 13, 32 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 13, 36 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 13, 40 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 14, 9 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 14, 15 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 14, 20 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 14, 35 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 14, 41 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 15, 14 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 15, 15 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 15, 34 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 15, 45 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 15, 48 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 16, 5 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 16, 25 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 16, 26 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 16, 45 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 16, 46 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 17, 8 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 17, 16 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 17, 25 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 17, 32 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 17, 48 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 18, 9 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 18, 11 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 18, 24 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 18, 35 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 18, 48 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 19, 17 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 19, 33 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 19, 37 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 19, 42 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 19, 46 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 20, 2 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 20, 15 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 20, 35 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 20, 38 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 20, 47 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 21, 9 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 21, 11 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 21, 31 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 21, 37 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 21, 43 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 22, 7 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 22, 13 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 22, 14 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 22, 25 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 22, 46 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 23, 3 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 23, 10 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 23, 11 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 23, 33 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 23, 48 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 24, 6 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 24, 7 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 24, 21 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 24, 36 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 24, 41 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 25, 5 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 25, 7 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 25, 11 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 25, 12 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 25, 24 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 26, 8 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 26, 10 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 26, 15 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 26, 36 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 26, 47 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 27, 12 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 27, 23 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 27, 24 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 27, 35 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 27, 44 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 28, 5 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 28, 15 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 28, 22 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 28, 27 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 28, 48 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 29, 1 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 29, 5 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 29, 11 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 29, 12 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 29, 23 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 30, 17 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 30, 27 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 30, 31 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 30, 41 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 30, 43 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 31, 10 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 31, 15 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 31, 19 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 31, 27 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 31, 33 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 32, 3 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 32, 33 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 32, 35 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 32, 45 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 32, 46 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 33, 6 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 33, 36 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 33, 38 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 33, 44 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 33, 47 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 34, 15 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 34, 25 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 34, 26 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 34, 35 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 34, 46 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 35, 18 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 35, 22 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 35, 32 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 35, 33 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 35, 41 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 36, 1 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 36, 24 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 36, 29 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 36, 31 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 36, 48 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 37, 3 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 37, 23 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 37, 33 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 37, 41 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 37, 44 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 38, 4 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 38, 26 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 38, 29 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 38, 35 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 38, 42 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 39, 9 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 39, 13 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 39, 16 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 39, 34 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 39, 45 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 40, 4 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 40, 10 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 40, 17 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 40, 24 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 40, 39 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 41, 19 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 41, 40 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 41, 41 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 41, 44 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 41, 48 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 42, 11 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 42, 19 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 42, 27 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 42, 34 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 42, 47 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 43, 15 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 43, 24 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 43, 26 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 43, 40 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 43, 45 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 44, 10 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 44, 11 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 44, 22 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 44, 28 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 44, 43 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 45, 12 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 45, 17 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 45, 23 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 45, 25 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 45, 31 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 46, 21 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 46, 24 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 46, 32 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 46, 37 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 46, 39 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 47, 3 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 47, 8 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 47, 23 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 47, 37 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 47, 40 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 48, 3 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 48, 27 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 48, 34 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 48, 37 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 48, 44 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 49, 9 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 49, 18 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 49, 38 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 49, 39 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 49, 42 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 50, 9 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 50, 17 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 50, 32 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 50, 37 });

            migrationBuilder.DeleteData(
                table: "ArtistaFilmes",
                keyColumns: new[] { "FilmeId", "ArtistaId" },
                keyValues: new object[] { 50, 47 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 1, 20 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 2, 23 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 3, 23 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 4, 11 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 5, 14 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 6, 20 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 7, 6 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 8, 19 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 9, 15 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 10, 27 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 11, 25 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 12, 13 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 13, 15 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 14, 24 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 15, 14 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 16, 1 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 17, 24 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 18, 14 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 19, 26 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 20, 21 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 21, 23 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 22, 19 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 23, 14 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 24, 27 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 25, 5 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 26, 19 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 27, 17 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 28, 27 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 29, 30 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 30, 25 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 31, 25 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 32, 8 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 33, 12 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 34, 14 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 35, 4 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 36, 22 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 37, 22 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 38, 22 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 39, 10 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 40, 13 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 41, 23 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 42, 3 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 43, 3 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 44, 9 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 45, 17 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 46, 29 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 47, 12 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 48, 1 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 49, 10 });

            migrationBuilder.DeleteData(
                table: "DiretorFilmes",
                keyColumns: new[] { "FilmeId", "DiretorId" },
                keyValues: new object[] { 50, 23 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 1, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 1, 9 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 1, 16 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 2, 11 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 3, 4 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 3, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 3, 15 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 4, 2 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 4, 5 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 4, 15 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 5, 2 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 5, 10 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 5, 11 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 6, 1 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 6, 12 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 6, 13 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 7, 1 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 7, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 7, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 8, 8 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 8, 12 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 8, 17 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 9, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 9, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 9, 10 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 10, 4 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 10, 9 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 10, 10 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 11, 5 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 11, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 11, 16 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 12, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 12, 15 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 12, 16 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 13, 2 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 13, 7 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 13, 15 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 14, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 14, 8 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 14, 9 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 15, 4 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 15, 8 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 15, 17 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 16, 7 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 16, 10 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 16, 17 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 17, 7 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 17, 8 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 17, 15 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 18, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 18, 9 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 18, 16 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 19, 1 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 19, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 19, 10 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 20, 10 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 20, 11 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 20, 13 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 21, 7 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 21, 12 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 21, 13 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 22, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 22, 11 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 22, 13 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 23, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 23, 8 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 23, 10 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 24, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 24, 10 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 24, 11 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 25, 2 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 25, 5 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 25, 9 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 26, 4 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 26, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 26, 17 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 27, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 27, 15 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 27, 17 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 28, 2 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 28, 14 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 28, 16 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 29, 1 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 29, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 29, 17 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 30, 5 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 30, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 30, 11 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 31, 9 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 31, 13 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 31, 16 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 32, 2 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 32, 12 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 32, 13 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 33, 4 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 33, 7 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 33, 9 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 34, 4 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 34, 14 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 34, 17 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 35, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 35, 11 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 35, 13 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 36, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 36, 7 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 36, 12 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 37, 4 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 37, 8 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 37, 14 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 38, 10 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 38, 11 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 38, 13 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 39, 10 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 39, 11 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 39, 14 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 40, 2 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 40, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 40, 17 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 41, 1 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 41, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 41, 13 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 42, 4 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 42, 8 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 42, 14 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 43, 7 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 43, 8 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 43, 17 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 44, 1 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 44, 7 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 44, 11 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 45, 4 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 45, 5 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 45, 14 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 46, 4 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 46, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 46, 9 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 47, 2 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 47, 6 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 47, 13 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 48, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 48, 9 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 48, 16 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 49, 2 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 49, 12 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 49, 13 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 50, 3 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 50, 14 });

            migrationBuilder.DeleteData(
                table: "GeneroFilmes",
                keyColumns: new[] { "FilmeId", "GeneroId" },
                keyValues: new object[] { 50, 15 });
        }
    }
}
