﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataSource.Migrations
{
    public partial class CriandoAdicionandoUsuarios : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Perfis",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Codigo = table.Column<string>(maxLength: 50, nullable: false),
                    Nome = table.Column<string>(maxLength: 50, nullable: false),
                    Ativo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Perfis", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 200, nullable: false),
                    Login = table.Column<string>(maxLength: 50, nullable: false),
                    Hash = table.Column<string>(maxLength: 88, nullable: false),
                    Salt = table.Column<string>(maxLength: 44, nullable: false),
                    PerfilId = table.Column<int>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Perfis_PerfilId",
                        column: x => x.PerfilId,
                        principalTable: "Perfis",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Perfis",
                columns: new[] { "Id", "Ativo", "Codigo", "Nome" },
                values: new object[] { 1, true, "ADMINISTRADOR", "Administrador" });

            migrationBuilder.InsertData(
                table: "Perfis",
                columns: new[] { "Id", "Ativo", "Codigo", "Nome" },
                values: new object[] { 2, true, "USUARIO", "Usuario" });

            migrationBuilder.InsertData(
                table: "Perfis",
                columns: new[] { "Id", "Ativo", "Codigo", "Nome" },
                values: new object[] { 3, true, "MASTER", "Master" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Ativo", "Hash", "Login", "Nome", "PerfilId", "Salt" },
                values: new object[] { 1, true, "9zoGLNZyu+zyT+LRogPVkUmBRRqyDb9cLgd2YgDrkiaPpv4EbkHamiCMN7yAZoTfMIJqBPK5IJX1voqumPlyFA==", "usuarioadmin", "Usuário Administrador da Silva", 1, "sVzkXqvO4nWSW5nV7mX/K1saq2IGvSOMjkcmV0m4NhM=" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Ativo", "Hash", "Login", "Nome", "PerfilId", "Salt" },
                values: new object[] { 2, true, "oEq8scYC3MfU5TMZ3c19HpNXKmi9sqybpNc0gRFP0jpBMbmA47ipuRYTqbXraGD6IydT6Edl3m/42e2faoCcaQ==", "usuariocomum", "Usuário Normal da Silva", 2, "OuH0/RVIyHQhcwFj8ncl15ykxNh1nJ4R29JCiXTyFGA=" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Ativo", "Hash", "Login", "Nome", "PerfilId", "Salt" },
                values: new object[] { 3, true, "DnYNvYU/zENgXotmFl1hJxNkGHIM3CD/gPTegHNd74xY37b1EHXdEuUKHf7/EtQhhUuMC7w/WeYEbjG+WPxdAg==", "usuariomestre", "Usuário Master da Silva", 3, "fyO+d8MkrKeHzteS5PB/3XTTOSVQbaw+KHL16h6Jq4k=" });

            migrationBuilder.CreateIndex(
                name: "IX_Users_PerfilId",
                table: "Users",
                column: "PerfilId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Perfis");
        }
    }
}
