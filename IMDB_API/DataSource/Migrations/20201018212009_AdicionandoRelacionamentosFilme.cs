﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataSource.Migrations
{
    public partial class AdicionandoRelacionamentosFilme : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Filmes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Filmes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ArtistaFilmes",
                columns: table => new
                {
                    FilmeId = table.Column<int>(nullable: false),
                    ArtistaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArtistaFilmes", x => new { x.FilmeId, x.ArtistaId });
                    table.ForeignKey(
                        name: "FK_ArtistaFilmes_Artistas_ArtistaId",
                        column: x => x.ArtistaId,
                        principalTable: "Artistas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ArtistaFilmes_Filmes_FilmeId",
                        column: x => x.FilmeId,
                        principalTable: "Filmes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DiretorFilmes",
                columns: table => new
                {
                    FilmeId = table.Column<int>(nullable: false),
                    DiretorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiretorFilmes", x => new { x.FilmeId, x.DiretorId });
                    table.ForeignKey(
                        name: "FK_DiretorFilmes_Diretores_DiretorId",
                        column: x => x.DiretorId,
                        principalTable: "Diretores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DiretorFilmes_Filmes_FilmeId",
                        column: x => x.FilmeId,
                        principalTable: "Filmes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "generoFilmes",
                columns: table => new
                {
                    FilmeId = table.Column<int>(nullable: false),
                    GeneroId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_generoFilmes", x => new { x.FilmeId, x.GeneroId });
                    table.ForeignKey(
                        name: "FK_generoFilmes_Filmes_FilmeId",
                        column: x => x.FilmeId,
                        principalTable: "Filmes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_generoFilmes_Generos_GeneroId",
                        column: x => x.GeneroId,
                        principalTable: "Generos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArtistaFilmes_ArtistaId",
                table: "ArtistaFilmes",
                column: "ArtistaId");

            migrationBuilder.CreateIndex(
                name: "IX_DiretorFilmes_DiretorId",
                table: "DiretorFilmes",
                column: "DiretorId");

            migrationBuilder.CreateIndex(
                name: "IX_generoFilmes_GeneroId",
                table: "generoFilmes",
                column: "GeneroId");

            migrationBuilder.AddForeignKey(
                name: "FK_AvaliacaoFilmes_Filmes_FilmeId",
                table: "AvaliacaoFilmes",
                column: "FilmeId",
                principalTable: "Filmes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AvaliacaoFilmes_Filmes_FilmeId",
                table: "AvaliacaoFilmes");

            migrationBuilder.DropTable(
                name: "ArtistaFilmes");

            migrationBuilder.DropTable(
                name: "DiretorFilmes");

            migrationBuilder.DropTable(
                name: "generoFilmes");

            migrationBuilder.DropTable(
                name: "Filmes");
        }
    }
}
