﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataSource.Migrations
{
    public partial class PopulandoFilmes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Ano",
                table: "Filmes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "Id", "Ano", "Nome" },
                values: new object[,]
                {
                    { 1, 1939, "The Wizard of Oz" },
                    { 28, 1941, "The Lady Eve" },
                    { 29, 1962, "Lawrence of Arabia" },
                    { 30, 1974, "Chinatown" },
                    { 31, 1954, "Rear Window" },
                    { 32, 1957, "12 Angry Men (Twelve Angry Men)" },
                    { 33, 1951, "A Streetcar Named Desire" },
                    { 34, 1954, "On the Waterfront" },
                    { 35, 1959, "The 400 Blows (Les Quatre cents coups)" },
                    { 36, 1938, "The Lady Vanishes" },
                    { 37, 1935, "The 39 Steps" },
                    { 38, 1948, "The Treasure of the Sierra Madre" },
                    { 39, 1949, "Kind Hearts and Coronets" },
                    { 40, 1964, "Dr. Strangelove Or How I Learned to Stop Worrying and Love the Bomb" },
                    { 41, 1931, "Frankenstein" },
                    { 42, 1951, "An American in Paris" },
                    { 43, 1958, "Touch of Evil" },
                    { 44, 1940, "His Girl Friday" },
                    { 45, 1935, "A Night at the Opera" },
                    { 46, 1951, "Rashômon" },
                    { 47, 1930, "All Quiet on the Western Front" },
                    { 48, 1925, "The Gold Rush" },
                    { 27, 1935, "The Bride of Frankenstein" },
                    { 26, 1959, "North by Northwest" },
                    { 25, 1940, "Rebecca" },
                    { 24, 1940, "The Philadelphia Story" },
                    { 2, 1941, "Citizen Kane" },
                    { 3, 1942, "Casablanca" },
                    { 4, 1936, "Modern Times" },
                    { 5, 1949, "The Third Man" },
                    { 6, 1920, "The Cabinet of Dr. Caligari (Das Cabinet des Dr. Caligari)" },
                    { 7, 1934, "It Happened One Night" },
                    { 8, 1938, "La Grande illusion (Grand Illusion)" },
                    { 9, 1937, "Snow White and the Seven Dwarfs" },
                    { 10, 1952, "Singin' in the Rain" },
                    { 11, 1921, "The Kid" },
                    { 49, 1940, "The Grapes of Wrath" },
                    { 12, 1950, "All About Eve" },
                    { 14, 1938, "The Adventures of Robin Hood" },
                    { 15, 1933, "King Kong" },
                    { 16, 1944, "Laura" },
                    { 17, 1922, "Nosferatu, a Symphony of Horror (Nosferatu, eine Symphonie des Grauens) (Nosferatu the Vampire)" },
                    { 18, 1950, "Sunset Boulevard" },
                    { 19, 1927, "Metropolis" },
                    { 20, 1960, "Psycho" },
                    { 21, 1935, "Top Hat" },
                    { 22, 1964, "A Hard Day's Night" },
                    { 23, 1956, "Seven Samurai (Shichinin no Samurai)" },
                    { 13, 1943, "Shadow of a Doubt" },
                    { 50, 1958, "Vertigo" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Filmes",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DropColumn(
                name: "Ano",
                table: "Filmes");
        }
    }
}
