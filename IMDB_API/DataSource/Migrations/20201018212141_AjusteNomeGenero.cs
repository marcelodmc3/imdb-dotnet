﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataSource.Migrations
{
    public partial class AjusteNomeGenero : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_generoFilmes_Filmes_FilmeId",
                table: "generoFilmes");

            migrationBuilder.DropForeignKey(
                name: "FK_generoFilmes_Generos_GeneroId",
                table: "generoFilmes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_generoFilmes",
                table: "generoFilmes");

            migrationBuilder.RenameTable(
                name: "generoFilmes",
                newName: "GeneroFilmes");

            migrationBuilder.RenameIndex(
                name: "IX_generoFilmes_GeneroId",
                table: "GeneroFilmes",
                newName: "IX_GeneroFilmes_GeneroId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GeneroFilmes",
                table: "GeneroFilmes",
                columns: new[] { "FilmeId", "GeneroId" });

            migrationBuilder.AddForeignKey(
                name: "FK_GeneroFilmes_Filmes_FilmeId",
                table: "GeneroFilmes",
                column: "FilmeId",
                principalTable: "Filmes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GeneroFilmes_Generos_GeneroId",
                table: "GeneroFilmes",
                column: "GeneroId",
                principalTable: "Generos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GeneroFilmes_Filmes_FilmeId",
                table: "GeneroFilmes");

            migrationBuilder.DropForeignKey(
                name: "FK_GeneroFilmes_Generos_GeneroId",
                table: "GeneroFilmes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GeneroFilmes",
                table: "GeneroFilmes");

            migrationBuilder.RenameTable(
                name: "GeneroFilmes",
                newName: "generoFilmes");

            migrationBuilder.RenameIndex(
                name: "IX_GeneroFilmes_GeneroId",
                table: "generoFilmes",
                newName: "IX_generoFilmes_GeneroId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_generoFilmes",
                table: "generoFilmes",
                columns: new[] { "FilmeId", "GeneroId" });

            migrationBuilder.AddForeignKey(
                name: "FK_generoFilmes_Filmes_FilmeId",
                table: "generoFilmes",
                column: "FilmeId",
                principalTable: "Filmes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_generoFilmes_Generos_GeneroId",
                table: "generoFilmes",
                column: "GeneroId",
                principalTable: "Generos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
