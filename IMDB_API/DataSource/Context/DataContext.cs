﻿using DataSource.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace DataSource.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options)
            : base(options)
        {
            //
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder()
                 .SetBasePath(Directory.GetCurrentDirectory())
                 .AddJsonFile("appsettings.json")
                 .Build();

            optionsBuilder.UseSqlServer(configuration.GetConnectionString("IMDB_DataBase"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Perfil>().HasData(
                new Perfil { Id = 1, Nome = "Administrador", Codigo = "ADMINISTRADOR", Ativo = true },
                new Perfil { Id = 2, Nome = "Usuario", Codigo = "USUARIO", Ativo = true });

            modelBuilder.Entity<Usuario>().HasData(
                new Usuario
                {
                    Id = 1,
                    Nome = "Usuário Administrador da Silva",
                    Login = "usuarioadmin",
                    Hash = "9zoGLNZyu+zyT+LRogPVkUmBRRqyDb9cLgd2YgDrkiaPpv4EbkHamiCMN7yAZoTfMIJqBPK5IJX1voqumPlyFA==",
                    Salt = "sVzkXqvO4nWSW5nV7mX/K1saq2IGvSOMjkcmV0m4NhM=",
                    Ativo = true,
                    PerfilId = 1
                },
                new Usuario
                {
                    Id = 2,
                    Nome = "Usuário Normal da Silva",
                    Login = "usuariocomum",
                    Hash = "oEq8scYC3MfU5TMZ3c19HpNXKmi9sqybpNc0gRFP0jpBMbmA47ipuRYTqbXraGD6IydT6Edl3m/42e2faoCcaQ==",
                    Salt = "OuH0/RVIyHQhcwFj8ncl15ykxNh1nJ4R29JCiXTyFGA=",
                    Ativo = true,
                    PerfilId = 2
                });


            modelBuilder.Entity<Artista>().HasData(
                new Artista { Id = 1, Nome = "Humphrey Bogart" }, new Artista { Id = 2, Nome = "Katharine Hepburn" }, new Artista { Id = 3, Nome = "Cary Grant" },
                new Artista { Id = 4, Nome = "Bette Davis" }, new Artista { Id = 5, Nome = "James Stewart" }, new Artista { Id = 6, Nome = "Audrey Hepburn" },
                new Artista { Id = 7, Nome = "Marlon Brando" }, new Artista { Id = 8, Nome = "Ingrid Bergman" }, new Artista { Id = 9, Nome = "Fred Astaire" },
                new Artista { Id = 10, Nome = "Greta Garbo" }, new Artista { Id = 11, Nome = "Henry Fonda" }, new Artista { Id = 12, Nome = "Marilyn Monroe" },
                new Artista { Id = 48, Nome = "Clark Gable" }, new Artista { Id = 13, Nome = "Elizabeth Taylor" }, new Artista { Id = 14, Nome = "James Cagney" },
                new Artista { Id = 15, Nome = "Judy Garland" }, new Artista { Id = 16, Nome = "Spencer Tracy" }, new Artista { Id = 17, Nome = "Marlene Dietrich" },
                new Artista { Id = 18, Nome = "Charlie Chaplin" }, new Artista { Id = 19, Nome = "Joan Crawford" }, new Artista { Id = 20, Nome = "Gary Cooper" },
                new Artista { Id = 21, Nome = "Barbara Stanwyck" }, new Artista { Id = 22, Nome = "Gregory Peck" }, new Artista { Id = 23, Nome = "Claudette Colbert" },
                new Artista { Id = 24, Nome = "John Wayne" }, new Artista { Id = 25, Nome = "Grace Kelly" }, new Artista { Id = 26, Nome = "Laurence Olivier" },
                new Artista { Id = 27, Nome = "Ginger Rogers" }, new Artista { Id = 28, Nome = "Gene Kelly" }, new Artista { Id = 29, Nome = "Mae West" },
                new Artista { Id = 30, Nome = "Orson Welles" }, new Artista { Id = 31, Nome = "Vivien Leigh" }, new Artista { Id = 32, Nome = "Kirk Douglas" },
                new Artista { Id = 33, Nome = "Lillian Gish" }, new Artista { Id = 34, Nome = "James Dean" }, new Artista { Id = 35, Nome = "Shirley Temple" },
                new Artista { Id = 36, Nome = "Burt Lancaster" }, new Artista { Id = 37, Nome = "Rita Hayworth" }, new Artista { Id = 38, Nome = "Marx Brothers" },
                new Artista { Id = 39, Nome = "Lauren Bacall" }, new Artista { Id = 40, Nome = "Buster Keaton" }, new Artista { Id = 41, Nome = "Sophia Loren" },
                new Artista { Id = 42, Nome = "Sidney Poitier" }, new Artista { Id = 43, Nome = "Jean Harlow" }, new Artista { Id = 44, Nome = "Robert Mitchum" },
                new Artista { Id = 45, Nome = "Carole Lombard" }, new Artista { Id = 46, Nome = "William Holden" }, new Artista { Id = 47, Nome = "Ava Gardner" });

            modelBuilder.Entity<ArtistaFilme>().HasKey(af => new { af.FilmeId, af.ArtistaId });
            modelBuilder.Entity<AvaliacaoFilme>().HasKey(af => new { af.FilmeId, af.UsuarioId });
            modelBuilder.Entity<DiretorFilme>().HasKey(df => new { df.FilmeId, df.DiretorId });
            modelBuilder.Entity<GeneroFilme>().HasKey(gf => new { gf.FilmeId, gf.GeneroId });

            modelBuilder.Entity<Filme>().HasMany(f => f.Avaliacoes)
                .WithOne()
                .HasForeignKey(a => a.FilmeId);

            modelBuilder.Entity<Filme>().HasMany(f => f.Diretores)
                .WithOne()
                .HasForeignKey(d => d.FilmeId);

            modelBuilder.Entity<Filme>().HasMany(f => f.Artistas)
                .WithOne()
                .HasForeignKey(a => a.FilmeId);

            modelBuilder.Entity<Filme>().HasMany(f => f.Generos)
                .WithOne()
                .HasForeignKey(g => g.FilmeId);

            modelBuilder.Entity<Filme>().HasData(
                new Filme { Id = 1, Nome = "The Wizard of Oz", Ano = 1939 }, new Filme { Id = 2, Nome = "Citizen Kane", Ano = 1941 },
                new Filme { Id = 3, Nome = "Casablanca", Ano = 1942 }, new Filme { Id = 4, Nome = "Modern Times", Ano = 1936 },
                new Filme { Id = 5, Nome = "The Third Man", Ano = 1949 }, new Filme { Id = 6, Nome = "The Cabinet of Dr. Caligari (Das Cabinet des Dr. Caligari)", Ano = 1920 },
                new Filme { Id = 7, Nome = "It Happened One Night", Ano = 1934 }, new Filme { Id = 8, Nome = "La Grande illusion (Grand Illusion)", Ano = 1938 },
                new Filme { Id = 9, Nome = "Snow White and the Seven Dwarfs", Ano = 1937 }, new Filme { Id = 10, Nome = "Singin' in the Rain", Ano = 1952 },
                new Filme { Id = 11, Nome = "The Kid", Ano = 1921 }, new Filme { Id = 12, Nome = "All About Eve", Ano = 1950 },
                new Filme { Id = 13, Nome = "Shadow of a Doubt", Ano = 1943 }, new Filme { Id = 14, Nome = "The Adventures of Robin Hood", Ano = 1938 },
                new Filme { Id = 15, Nome = "King Kong", Ano = 1933 }, new Filme { Id = 16, Nome = "Laura", Ano = 1944 },
                new Filme { Id = 17, Nome = "Nosferatu, a Symphony of Horror (Nosferatu, eine Symphonie des Grauens) (Nosferatu the Vampire)", Ano = 1922 },
                new Filme { Id = 18, Nome = "Sunset Boulevard", Ano = 1950 }, new Filme { Id = 19, Nome = "Metropolis", Ano = 1927 },
                new Filme { Id = 20, Nome = "Psycho", Ano = 1960 }, new Filme { Id = 21, Nome = "Top Hat", Ano = 1935 },
                new Filme { Id = 22, Nome = "A Hard Day's Night", Ano = 1964 }, new Filme { Id = 23, Nome = "Seven Samurai (Shichinin no Samurai)", Ano = 1956 },
                new Filme { Id = 24, Nome = "The Philadelphia Story", Ano = 1940 }, new Filme { Id = 25, Nome = "Rebecca", Ano = 1940 },
                new Filme { Id = 26, Nome = "North by Northwest", Ano = 1959 }, new Filme { Id = 27, Nome = "The Bride of Frankenstein", Ano = 1935 },
                new Filme { Id = 28, Nome = "The Lady Eve", Ano = 1941 }, new Filme { Id = 29, Nome = "Lawrence of Arabia", Ano = 1962 },
                new Filme { Id = 30, Nome = "Chinatown", Ano = 1974 }, new Filme { Id = 31, Nome = "Rear Window", Ano = 1954 },
                new Filme { Id = 32, Nome = "12 Angry Men (Twelve Angry Men)", Ano = 1957 }, new Filme { Id = 33, Nome = "A Streetcar Named Desire", Ano = 1951 },
                new Filme { Id = 34, Nome = "On the Waterfront", Ano = 1954 }, new Filme { Id = 35, Nome = "The 400 Blows (Les Quatre cents coups)", Ano = 1959 },
                new Filme { Id = 36, Nome = "The Lady Vanishes", Ano = 1938 }, new Filme { Id = 37, Nome = "The 39 Steps", Ano = 1935 },
                new Filme { Id = 38, Nome = "The Treasure of the Sierra Madre", Ano = 1948 }, new Filme { Id = 39, Nome = "Kind Hearts and Coronets", Ano = 1949 },
                new Filme { Id = 40, Nome = "Dr. Strangelove Or How I Learned to Stop Worrying and Love the Bomb", Ano = 1964 }, new Filme { Id = 41, Nome = "Frankenstein", Ano = 1931 },
                new Filme { Id = 42, Nome = "An American in Paris", Ano = 1951 }, new Filme { Id = 43, Nome = "Touch of Evil", Ano = 1958 },
                new Filme { Id = 44, Nome = "His Girl Friday", Ano = 1940 }, new Filme { Id = 45, Nome = "A Night at the Opera", Ano = 1935 },
                new Filme { Id = 46, Nome = "Rashômon", Ano = 1951 }, new Filme { Id = 47, Nome = "All Quiet on the Western Front", Ano = 1930 },
                new Filme { Id = 48, Nome = "The Gold Rush", Ano = 1925 }, new Filme { Id = 49, Nome = "The Grapes of Wrath", Ano = 1940 },
                new Filme { Id = 50, Nome = "Vertigo", Ano = 1958 });

            modelBuilder.Entity<Genero>().HasData(
                new Genero { Id = 1, Nome = "Ação" }, new Genero { Id = 2, Nome = "Animação" }, new Genero { Id = 3, Nome = "Aventura" },
                new Genero { Id = 4, Nome = "Cinema de arte" }, new Genero { Id = 5, Nome = "Chanchada" }, new Genero { Id = 6, Nome = "Comédia" },
                new Genero { Id = 7, Nome = "Comédia de ação" }, new Genero { Id = 8, Nome = "Comédia de terror" }, new Genero { Id = 9, Nome = "Comédia dramática" },
                new Genero { Id = 10, Nome = "Comédia romântica" }, new Genero { Id = 11, Nome = "Dança" }, new Genero { Id = 12, Nome = "Documentário" },
                new Genero { Id = 13, Nome = "Docuficção" }, new Genero { Id = 14, Nome = "Drama" }, new Genero { Id = 15, Nome = "Espionagem" },
                new Genero { Id = 16, Nome = "Faroeste" }, new Genero { Id = 17, Nome = "Fantasia científica" }, new Genero { Id = 18, Nome = "Ficção científica" },
                new Genero { Id = 19, Nome = "Filmes com truques" }, new Genero { Id = 20, Nome = "Filmes de guerra" }, new Genero { Id = 21, Nome = "Musical" },
                new Genero { Id = 22, Nome = "Filme policial" }, new Genero { Id = 23, Nome = "Romance" }, new Genero { Id = 24, Nome = "Seriado" },
                new Genero { Id = 25, Nome = "Suspense" }, new Genero { Id = 26, Nome = "Terror" }, new Genero { Id = 27, Nome = "Thriller" });

            modelBuilder.Entity<Diretor>().HasData(
                new Diretor { Id = 1, Nome = "Alfred Hitchcock" }, new Diretor { Id = 2, Nome = "Orson Welles" },
                new Diretor { Id = 3, Nome = "John Ford" }, new Diretor { Id = 4, Nome = "Howard Hawks" },
                new Diretor { Id = 5, Nome = "Martin Scorsese" }, new Diretor { Id = 6, Nome = "Akira Kurosawa" },
                new Diretor { Id = 7, Nome = "Buster Keaton" }, new Diretor { Id = 8, Nome = "Ingmar Bergman" },
                new Diretor { Id = 9, Nome = "Frank Capra" }, new Diretor { Id = 10, Nome = "Federico Fellini" },
                new Diretor { Id = 11, Nome = "Steven Spielberg" }, new Diretor { Id = 12, Nome = "Jean Renoir" },
                new Diretor { Id = 13, Nome = "John Huston" }, new Diretor { Id = 14, Nome = "Luis Buñuel" },
                new Diretor { Id = 15, Nome = "D. W. Griffith" }, new Diretor { Id = 16, Nome = "Ernst Lubitsch" },
                new Diretor { Id = 17, Nome = "Robert Altman" }, new Diretor { Id = 18, Nome = "George Cukor" },
                new Diretor { Id = 19, Nome = "Woody Allen" }, new Diretor { Id = 20, Nome = "Vincente Minnelli" },
                new Diretor { Id = 21, Nome = "Francis Ford Coppola" }, new Diretor { Id = 22, Nome = "Michael Powell" },
                new Diretor { Id = 23, Nome = "Stanley Kubrick" }, new Diretor { Id = 24, Nome = "Billy Wilder" },
                new Diretor { Id = 25, Nome = "Satyajit Ray" }, new Diretor { Id = 26, Nome = "Roman Polanski" },
                new Diretor { Id = 27, Nome = "François Truffaut" }, new Diretor { Id = 28, Nome = "Preston Sturges" },
                new Diretor { Id = 29, Nome = "Sergei Eisenstein" }, new Diretor { Id = 30, Nome = "Fritz Lang" });

            modelBuilder.Entity<ArtistaFilme>().HasData(
                new ArtistaFilme() { FilmeId = 1, ArtistaId = 12 }, new ArtistaFilme() { FilmeId = 1, ArtistaId = 22 }, new ArtistaFilme() { FilmeId = 1, ArtistaId = 6 }, new ArtistaFilme() { FilmeId = 1, ArtistaId = 11 }, new ArtistaFilme() { FilmeId = 1, ArtistaId = 14 },
                new ArtistaFilme() { FilmeId = 2, ArtistaId = 47 }, new ArtistaFilme() { FilmeId = 2, ArtistaId = 26 }, new ArtistaFilme() { FilmeId = 2, ArtistaId = 5 }, new ArtistaFilme() { FilmeId = 2, ArtistaId = 38 }, new ArtistaFilme() { FilmeId = 2, ArtistaId = 27 },
                new ArtistaFilme() { FilmeId = 3, ArtistaId = 44 }, new ArtistaFilme() { FilmeId = 3, ArtistaId = 45 }, new ArtistaFilme() { FilmeId = 3, ArtistaId = 36 }, new ArtistaFilme() { FilmeId = 3, ArtistaId = 3 }, new ArtistaFilme() { FilmeId = 3, ArtistaId = 22 },
                new ArtistaFilme() { FilmeId = 4, ArtistaId = 10 }, new ArtistaFilme() { FilmeId = 4, ArtistaId = 2 }, new ArtistaFilme() { FilmeId = 4, ArtistaId = 1 }, new ArtistaFilme() { FilmeId = 4, ArtistaId = 36 }, new ArtistaFilme() { FilmeId = 4, ArtistaId = 27 },
                new ArtistaFilme() { FilmeId = 5, ArtistaId = 2 }, new ArtistaFilme() { FilmeId = 5, ArtistaId = 44 }, new ArtistaFilme() { FilmeId = 5, ArtistaId = 12 }, new ArtistaFilme() { FilmeId = 5, ArtistaId = 27 }, new ArtistaFilme() { FilmeId = 5, ArtistaId = 6 },
                new ArtistaFilme() { FilmeId = 6, ArtistaId = 40 }, new ArtistaFilme() { FilmeId = 6, ArtistaId = 21 }, new ArtistaFilme() { FilmeId = 6, ArtistaId = 12 }, new ArtistaFilme() { FilmeId = 6, ArtistaId = 20 }, new ArtistaFilme() { FilmeId = 6, ArtistaId = 44 },
                new ArtistaFilme() { FilmeId = 7, ArtistaId = 16 }, new ArtistaFilme() { FilmeId = 7, ArtistaId = 10 }, new ArtistaFilme() { FilmeId = 7, ArtistaId = 44 }, new ArtistaFilme() { FilmeId = 7, ArtistaId = 11 }, new ArtistaFilme() { FilmeId = 7, ArtistaId = 2 },
                new ArtistaFilme() { FilmeId = 8, ArtistaId = 3 }, new ArtistaFilme() { FilmeId = 8, ArtistaId = 43 }, new ArtistaFilme() { FilmeId = 8, ArtistaId = 42 }, new ArtistaFilme() { FilmeId = 8, ArtistaId = 1 }, new ArtistaFilme() { FilmeId = 8, ArtistaId = 21 },
                new ArtistaFilme() { FilmeId = 9, ArtistaId = 31 }, new ArtistaFilme() { FilmeId = 9, ArtistaId = 16 }, new ArtistaFilme() { FilmeId = 9, ArtistaId = 15 }, new ArtistaFilme() { FilmeId = 9, ArtistaId = 2 }, new ArtistaFilme() { FilmeId = 9, ArtistaId = 30 },
                new ArtistaFilme() { FilmeId = 10, ArtistaId = 42 }, new ArtistaFilme() { FilmeId = 10, ArtistaId = 17 }, new ArtistaFilme() { FilmeId = 10, ArtistaId = 28 }, new ArtistaFilme() { FilmeId = 10, ArtistaId = 4 }, new ArtistaFilme() { FilmeId = 10, ArtistaId = 19 },
                new ArtistaFilme() { FilmeId = 11, ArtistaId = 8 }, new ArtistaFilme() { FilmeId = 11, ArtistaId = 10 }, new ArtistaFilme() { FilmeId = 11, ArtistaId = 11 }, new ArtistaFilme() { FilmeId = 11, ArtistaId = 27 }, new ArtistaFilme() { FilmeId = 11, ArtistaId = 42 },
                new ArtistaFilme() { FilmeId = 12, ArtistaId = 32 }, new ArtistaFilme() { FilmeId = 12, ArtistaId = 9 }, new ArtistaFilme() { FilmeId = 12, ArtistaId = 8 }, new ArtistaFilme() { FilmeId = 12, ArtistaId = 5 }, new ArtistaFilme() { FilmeId = 12, ArtistaId = 42 },
                new ArtistaFilme() { FilmeId = 13, ArtistaId = 19 }, new ArtistaFilme() { FilmeId = 13, ArtistaId = 32 }, new ArtistaFilme() { FilmeId = 13, ArtistaId = 29 }, new ArtistaFilme() { FilmeId = 13, ArtistaId = 36 }, new ArtistaFilme() { FilmeId = 13, ArtistaId = 40 },
                new ArtistaFilme() { FilmeId = 14, ArtistaId = 41 }, new ArtistaFilme() { FilmeId = 14, ArtistaId = 35 }, new ArtistaFilme() { FilmeId = 14, ArtistaId = 20 }, new ArtistaFilme() { FilmeId = 14, ArtistaId = 15 }, new ArtistaFilme() { FilmeId = 14, ArtistaId = 9 },
                new ArtistaFilme() { FilmeId = 15, ArtistaId = 48 }, new ArtistaFilme() { FilmeId = 15, ArtistaId = 15 }, new ArtistaFilme() { FilmeId = 15, ArtistaId = 45 }, new ArtistaFilme() { FilmeId = 15, ArtistaId = 34 }, new ArtistaFilme() { FilmeId = 15, ArtistaId = 14 },
                new ArtistaFilme() { FilmeId = 16, ArtistaId = 45 }, new ArtistaFilme() { FilmeId = 16, ArtistaId = 26 }, new ArtistaFilme() { FilmeId = 16, ArtistaId = 25 }, new ArtistaFilme() { FilmeId = 16, ArtistaId = 46 }, new ArtistaFilme() { FilmeId = 16, ArtistaId = 5 },
                new ArtistaFilme() { FilmeId = 17, ArtistaId = 32 }, new ArtistaFilme() { FilmeId = 17, ArtistaId = 16 }, new ArtistaFilme() { FilmeId = 17, ArtistaId = 25 }, new ArtistaFilme() { FilmeId = 17, ArtistaId = 8 }, new ArtistaFilme() { FilmeId = 17, ArtistaId = 48 },
                new ArtistaFilme() { FilmeId = 18, ArtistaId = 35 }, new ArtistaFilme() { FilmeId = 18, ArtistaId = 48 }, new ArtistaFilme() { FilmeId = 18, ArtistaId = 24 }, new ArtistaFilme() { FilmeId = 18, ArtistaId = 9 }, new ArtistaFilme() { FilmeId = 18, ArtistaId = 11 },
                new ArtistaFilme() { FilmeId = 19, ArtistaId = 33 }, new ArtistaFilme() { FilmeId = 19, ArtistaId = 42 }, new ArtistaFilme() { FilmeId = 19, ArtistaId = 17 }, new ArtistaFilme() { FilmeId = 19, ArtistaId = 37 }, new ArtistaFilme() { FilmeId = 19, ArtistaId = 46 },
                new ArtistaFilme() { FilmeId = 20, ArtistaId = 47 }, new ArtistaFilme() { FilmeId = 20, ArtistaId = 35 }, new ArtistaFilme() { FilmeId = 20, ArtistaId = 15 }, new ArtistaFilme() { FilmeId = 20, ArtistaId = 38 }, new ArtistaFilme() { FilmeId = 20, ArtistaId = 2 },
                new ArtistaFilme() { FilmeId = 21, ArtistaId = 31 }, new ArtistaFilme() { FilmeId = 21, ArtistaId = 9 }, new ArtistaFilme() { FilmeId = 21, ArtistaId = 37 }, new ArtistaFilme() { FilmeId = 21, ArtistaId = 11 }, new ArtistaFilme() { FilmeId = 21, ArtistaId = 43 },
                new ArtistaFilme() { FilmeId = 22, ArtistaId = 14 }, new ArtistaFilme() { FilmeId = 22, ArtistaId = 25 }, new ArtistaFilme() { FilmeId = 22, ArtistaId = 13 }, new ArtistaFilme() { FilmeId = 22, ArtistaId = 46 }, new ArtistaFilme() { FilmeId = 22, ArtistaId = 7 },
                new ArtistaFilme() { FilmeId = 23, ArtistaId = 33 }, new ArtistaFilme() { FilmeId = 23, ArtistaId = 11 }, new ArtistaFilme() { FilmeId = 23, ArtistaId = 48 }, new ArtistaFilme() { FilmeId = 23, ArtistaId = 10 }, new ArtistaFilme() { FilmeId = 23, ArtistaId = 3 },
                new ArtistaFilme() { FilmeId = 24, ArtistaId = 7 }, new ArtistaFilme() { FilmeId = 24, ArtistaId = 21 }, new ArtistaFilme() { FilmeId = 24, ArtistaId = 36 }, new ArtistaFilme() { FilmeId = 24, ArtistaId = 6 }, new ArtistaFilme() { FilmeId = 24, ArtistaId = 41 },
                new ArtistaFilme() { FilmeId = 25, ArtistaId = 12 }, new ArtistaFilme() { FilmeId = 25, ArtistaId = 11 }, new ArtistaFilme() { FilmeId = 25, ArtistaId = 5 }, new ArtistaFilme() { FilmeId = 25, ArtistaId = 24 }, new ArtistaFilme() { FilmeId = 25, ArtistaId = 7 },
                new ArtistaFilme() { FilmeId = 26, ArtistaId = 47 }, new ArtistaFilme() { FilmeId = 26, ArtistaId = 8 }, new ArtistaFilme() { FilmeId = 26, ArtistaId = 36 }, new ArtistaFilme() { FilmeId = 26, ArtistaId = 15 }, new ArtistaFilme() { FilmeId = 26, ArtistaId = 10 },
                new ArtistaFilme() { FilmeId = 27, ArtistaId = 12 }, new ArtistaFilme() { FilmeId = 27, ArtistaId = 35 }, new ArtistaFilme() { FilmeId = 27, ArtistaId = 44 }, new ArtistaFilme() { FilmeId = 27, ArtistaId = 23 }, new ArtistaFilme() { FilmeId = 27, ArtistaId = 24 },
                new ArtistaFilme() { FilmeId = 28, ArtistaId = 27 }, new ArtistaFilme() { FilmeId = 28, ArtistaId = 15 }, new ArtistaFilme() { FilmeId = 28, ArtistaId = 48 }, new ArtistaFilme() { FilmeId = 28, ArtistaId = 5 }, new ArtistaFilme() { FilmeId = 28, ArtistaId = 22 },
                new ArtistaFilme() { FilmeId = 29, ArtistaId = 23 }, new ArtistaFilme() { FilmeId = 29, ArtistaId = 11 }, new ArtistaFilme() { FilmeId = 29, ArtistaId = 12 }, new ArtistaFilme() { FilmeId = 29, ArtistaId = 5 }, new ArtistaFilme() { FilmeId = 29, ArtistaId = 1 },
                new ArtistaFilme() { FilmeId = 30, ArtistaId = 31 }, new ArtistaFilme() { FilmeId = 30, ArtistaId = 17 }, new ArtistaFilme() { FilmeId = 30, ArtistaId = 27 }, new ArtistaFilme() { FilmeId = 30, ArtistaId = 43 }, new ArtistaFilme() { FilmeId = 30, ArtistaId = 41 },
                new ArtistaFilme() { FilmeId = 31, ArtistaId = 15 }, new ArtistaFilme() { FilmeId = 31, ArtistaId = 27 }, new ArtistaFilme() { FilmeId = 31, ArtistaId = 33 }, new ArtistaFilme() { FilmeId = 31, ArtistaId = 10 }, new ArtistaFilme() { FilmeId = 31, ArtistaId = 19 },
                new ArtistaFilme() { FilmeId = 32, ArtistaId = 46 }, new ArtistaFilme() { FilmeId = 32, ArtistaId = 33 }, new ArtistaFilme() { FilmeId = 32, ArtistaId = 35 }, new ArtistaFilme() { FilmeId = 32, ArtistaId = 3 }, new ArtistaFilme() { FilmeId = 32, ArtistaId = 45 },
                new ArtistaFilme() { FilmeId = 33, ArtistaId = 6 }, new ArtistaFilme() { FilmeId = 33, ArtistaId = 38 }, new ArtistaFilme() { FilmeId = 33, ArtistaId = 47 }, new ArtistaFilme() { FilmeId = 33, ArtistaId = 36 }, new ArtistaFilme() { FilmeId = 33, ArtistaId = 44 },
                new ArtistaFilme() { FilmeId = 34, ArtistaId = 46 }, new ArtistaFilme() { FilmeId = 34, ArtistaId = 15 }, new ArtistaFilme() { FilmeId = 34, ArtistaId = 26 }, new ArtistaFilme() { FilmeId = 34, ArtistaId = 35 }, new ArtistaFilme() { FilmeId = 34, ArtistaId = 25 },
                new ArtistaFilme() { FilmeId = 35, ArtistaId = 41 }, new ArtistaFilme() { FilmeId = 35, ArtistaId = 22 }, new ArtistaFilme() { FilmeId = 35, ArtistaId = 32 }, new ArtistaFilme() { FilmeId = 35, ArtistaId = 33 }, new ArtistaFilme() { FilmeId = 35, ArtistaId = 18 },
                new ArtistaFilme() { FilmeId = 36, ArtistaId = 1 }, new ArtistaFilme() { FilmeId = 36, ArtistaId = 48 }, new ArtistaFilme() { FilmeId = 36, ArtistaId = 24 }, new ArtistaFilme() { FilmeId = 36, ArtistaId = 31 }, new ArtistaFilme() { FilmeId = 36, ArtistaId = 29 },
                new ArtistaFilme() { FilmeId = 37, ArtistaId = 41 }, new ArtistaFilme() { FilmeId = 37, ArtistaId = 23 }, new ArtistaFilme() { FilmeId = 37, ArtistaId = 44 }, new ArtistaFilme() { FilmeId = 37, ArtistaId = 33 }, new ArtistaFilme() { FilmeId = 37, ArtistaId = 3 },
                new ArtistaFilme() { FilmeId = 38, ArtistaId = 35 }, new ArtistaFilme() { FilmeId = 38, ArtistaId = 4 }, new ArtistaFilme() { FilmeId = 38, ArtistaId = 26 }, new ArtistaFilme() { FilmeId = 38, ArtistaId = 42 }, new ArtistaFilme() { FilmeId = 38, ArtistaId = 29 },
                new ArtistaFilme() { FilmeId = 39, ArtistaId = 45 }, new ArtistaFilme() { FilmeId = 39, ArtistaId = 9 }, new ArtistaFilme() { FilmeId = 39, ArtistaId = 16 }, new ArtistaFilme() { FilmeId = 39, ArtistaId = 34 }, new ArtistaFilme() { FilmeId = 39, ArtistaId = 13 },
                new ArtistaFilme() { FilmeId = 40, ArtistaId = 24 }, new ArtistaFilme() { FilmeId = 40, ArtistaId = 10 }, new ArtistaFilme() { FilmeId = 40, ArtistaId = 4 }, new ArtistaFilme() { FilmeId = 40, ArtistaId = 17 }, new ArtistaFilme() { FilmeId = 40, ArtistaId = 39 },
                new ArtistaFilme() { FilmeId = 41, ArtistaId = 41 }, new ArtistaFilme() { FilmeId = 41, ArtistaId = 19 }, new ArtistaFilme() { FilmeId = 41, ArtistaId = 48 }, new ArtistaFilme() { FilmeId = 41, ArtistaId = 44 }, new ArtistaFilme() { FilmeId = 41, ArtistaId = 40 },
                new ArtistaFilme() { FilmeId = 42, ArtistaId = 19 }, new ArtistaFilme() { FilmeId = 42, ArtistaId = 47 }, new ArtistaFilme() { FilmeId = 42, ArtistaId = 34 }, new ArtistaFilme() { FilmeId = 42, ArtistaId = 11 }, new ArtistaFilme() { FilmeId = 42, ArtistaId = 27 },
                new ArtistaFilme() { FilmeId = 43, ArtistaId = 15 }, new ArtistaFilme() { FilmeId = 43, ArtistaId = 26 }, new ArtistaFilme() { FilmeId = 43, ArtistaId = 45 }, new ArtistaFilme() { FilmeId = 43, ArtistaId = 24 }, new ArtistaFilme() { FilmeId = 43, ArtistaId = 40 },
                new ArtistaFilme() { FilmeId = 44, ArtistaId = 11 }, new ArtistaFilme() { FilmeId = 44, ArtistaId = 43 }, new ArtistaFilme() { FilmeId = 44, ArtistaId = 28 }, new ArtistaFilme() { FilmeId = 44, ArtistaId = 22 }, new ArtistaFilme() { FilmeId = 44, ArtistaId = 10 },
                new ArtistaFilme() { FilmeId = 45, ArtistaId = 31 }, new ArtistaFilme() { FilmeId = 45, ArtistaId = 12 }, new ArtistaFilme() { FilmeId = 45, ArtistaId = 25 }, new ArtistaFilme() { FilmeId = 45, ArtistaId = 23 }, new ArtistaFilme() { FilmeId = 45, ArtistaId = 17 },
                new ArtistaFilme() { FilmeId = 46, ArtistaId = 32 }, new ArtistaFilme() { FilmeId = 46, ArtistaId = 37 }, new ArtistaFilme() { FilmeId = 46, ArtistaId = 24 }, new ArtistaFilme() { FilmeId = 46, ArtistaId = 39 }, new ArtistaFilme() { FilmeId = 46, ArtistaId = 21 },
                new ArtistaFilme() { FilmeId = 47, ArtistaId = 8 }, new ArtistaFilme() { FilmeId = 47, ArtistaId = 40 }, new ArtistaFilme() { FilmeId = 47, ArtistaId = 37 }, new ArtistaFilme() { FilmeId = 47, ArtistaId = 23 }, new ArtistaFilme() { FilmeId = 47, ArtistaId = 3 },
                new ArtistaFilme() { FilmeId = 48, ArtistaId = 37 }, new ArtistaFilme() { FilmeId = 48, ArtistaId = 3 }, new ArtistaFilme() { FilmeId = 48, ArtistaId = 44 }, new ArtistaFilme() { FilmeId = 48, ArtistaId = 34 }, new ArtistaFilme() { FilmeId = 48, ArtistaId = 27 },
                new ArtistaFilme() { FilmeId = 49, ArtistaId = 9 }, new ArtistaFilme() { FilmeId = 49, ArtistaId = 18 }, new ArtistaFilme() { FilmeId = 49, ArtistaId = 42 }, new ArtistaFilme() { FilmeId = 49, ArtistaId = 39 }, new ArtistaFilme() { FilmeId = 49, ArtistaId = 38 },
                new ArtistaFilme() { FilmeId = 50, ArtistaId = 37 }, new ArtistaFilme() { FilmeId = 50, ArtistaId = 32 }, new ArtistaFilme() { FilmeId = 50, ArtistaId = 47 }, new ArtistaFilme() { FilmeId = 50, ArtistaId = 9 }, new ArtistaFilme() { FilmeId = 50, ArtistaId = 17 });

            modelBuilder.Entity<DiretorFilme>().HasData(
                new DiretorFilme() { FilmeId = 1, DiretorId = 20 }, new DiretorFilme() { FilmeId = 2, DiretorId = 23 },
                new DiretorFilme() { FilmeId = 3, DiretorId = 23 }, new DiretorFilme() { FilmeId = 4, DiretorId = 11 },
                new DiretorFilme() { FilmeId = 5, DiretorId = 14 }, new DiretorFilme() { FilmeId = 6, DiretorId = 20 },
                new DiretorFilme() { FilmeId = 7, DiretorId = 6 }, new DiretorFilme() { FilmeId = 8, DiretorId = 19 },
                new DiretorFilme() { FilmeId = 9, DiretorId = 15 }, new DiretorFilme() { FilmeId = 10, DiretorId = 27 },
                new DiretorFilme() { FilmeId = 11, DiretorId = 25 }, new DiretorFilme() { FilmeId = 12, DiretorId = 13 },
                new DiretorFilme() { FilmeId = 13, DiretorId = 15 }, new DiretorFilme() { FilmeId = 14, DiretorId = 24 },
                new DiretorFilme() { FilmeId = 15, DiretorId = 14 }, new DiretorFilme() { FilmeId = 16, DiretorId = 1 },
                new DiretorFilme() { FilmeId = 17, DiretorId = 24 }, new DiretorFilme() { FilmeId = 18, DiretorId = 14 },
                new DiretorFilme() { FilmeId = 19, DiretorId = 26 }, new DiretorFilme() { FilmeId = 20, DiretorId = 21 },
                new DiretorFilme() { FilmeId = 21, DiretorId = 23 }, new DiretorFilme() { FilmeId = 22, DiretorId = 19 },
                new DiretorFilme() { FilmeId = 23, DiretorId = 14 }, new DiretorFilme() { FilmeId = 24, DiretorId = 27 },
                new DiretorFilme() { FilmeId = 25, DiretorId = 5 }, new DiretorFilme() { FilmeId = 26, DiretorId = 19 },
                new DiretorFilme() { FilmeId = 27, DiretorId = 17 }, new DiretorFilme() { FilmeId = 28, DiretorId = 27 },
                new DiretorFilme() { FilmeId = 29, DiretorId = 30 }, new DiretorFilme() { FilmeId = 30, DiretorId = 25 },
                new DiretorFilme() { FilmeId = 31, DiretorId = 25 }, new DiretorFilme() { FilmeId = 32, DiretorId = 8 },
                new DiretorFilme() { FilmeId = 33, DiretorId = 12 }, new DiretorFilme() { FilmeId = 34, DiretorId = 14 },
                new DiretorFilme() { FilmeId = 35, DiretorId = 4 }, new DiretorFilme() { FilmeId = 36, DiretorId = 22 },
                new DiretorFilme() { FilmeId = 37, DiretorId = 22 }, new DiretorFilme() { FilmeId = 38, DiretorId = 22 },
                new DiretorFilme() { FilmeId = 39, DiretorId = 10 }, new DiretorFilme() { FilmeId = 40, DiretorId = 13 },
                new DiretorFilme() { FilmeId = 41, DiretorId = 23 }, new DiretorFilme() { FilmeId = 42, DiretorId = 3 },
                new DiretorFilme() { FilmeId = 43, DiretorId = 3 }, new DiretorFilme() { FilmeId = 44, DiretorId = 9 },
                new DiretorFilme() { FilmeId = 45, DiretorId = 17 }, new DiretorFilme() { FilmeId = 46, DiretorId = 29 },
                new DiretorFilme() { FilmeId = 47, DiretorId = 12 }, new DiretorFilme() { FilmeId = 48, DiretorId = 1 },
                new DiretorFilme() { FilmeId = 49, DiretorId = 10 }, new DiretorFilme() { FilmeId = 50, DiretorId = 23 });

            modelBuilder.Entity<GeneroFilme>().HasData(
                new GeneroFilme() { FilmeId = 1, GeneroId = 16 }, new GeneroFilme() { FilmeId = 1, GeneroId = 9 }, new GeneroFilme() { FilmeId = 1, GeneroId = 6 },
                new GeneroFilme() { FilmeId = 2, GeneroId = 2 }, new GeneroFilme() { FilmeId = 2, GeneroId = 11 }, new GeneroFilme() { FilmeId = 2, GeneroId = 1 },
                new GeneroFilme() { FilmeId = 3, GeneroId = 6 }, new GeneroFilme() { FilmeId = 3, GeneroId = 15 }, new GeneroFilme() { FilmeId = 3, GeneroId = 4 },
                new GeneroFilme() { FilmeId = 4, GeneroId = 15 }, new GeneroFilme() { FilmeId = 4, GeneroId = 2 }, new GeneroFilme() { FilmeId = 4, GeneroId = 5 },
                new GeneroFilme() { FilmeId = 5, GeneroId = 11 }, new GeneroFilme() { FilmeId = 5, GeneroId = 2 }, new GeneroFilme() { FilmeId = 5, GeneroId = 10 },
                new GeneroFilme() { FilmeId = 6, GeneroId = 1 }, new GeneroFilme() { FilmeId = 6, GeneroId = 12 }, new GeneroFilme() { FilmeId = 6, GeneroId = 13 },
                new GeneroFilme() { FilmeId = 7, GeneroId = 6 }, new GeneroFilme() { FilmeId = 7, GeneroId = 1 }, new GeneroFilme() { FilmeId = 7, GeneroId = 3 },
                new GeneroFilme() { FilmeId = 8, GeneroId = 8 }, new GeneroFilme() { FilmeId = 8, GeneroId = 17 }, new GeneroFilme() { FilmeId = 8, GeneroId = 12 },
                new GeneroFilme() { FilmeId = 9, GeneroId = 10 }, new GeneroFilme() { FilmeId = 9, GeneroId = 3 }, new GeneroFilme() { FilmeId = 9, GeneroId = 6 },
                new GeneroFilme() { FilmeId = 10, GeneroId = 4 }, new GeneroFilme() { FilmeId = 10, GeneroId = 9 }, new GeneroFilme() { FilmeId = 10, GeneroId = 10 },
                new GeneroFilme() { FilmeId = 11, GeneroId = 5 }, new GeneroFilme() { FilmeId = 11, GeneroId = 16 }, new GeneroFilme() { FilmeId = 11, GeneroId = 6 },
                new GeneroFilme() { FilmeId = 12, GeneroId = 16 }, new GeneroFilme() { FilmeId = 12, GeneroId = 6 }, new GeneroFilme() { FilmeId = 12, GeneroId = 15 },
                new GeneroFilme() { FilmeId = 13, GeneroId = 2 }, new GeneroFilme() { FilmeId = 13, GeneroId = 7 }, new GeneroFilme() { FilmeId = 13, GeneroId = 15 },
                new GeneroFilme() { FilmeId = 14, GeneroId = 9 }, new GeneroFilme() { FilmeId = 14, GeneroId = 6 }, new GeneroFilme() { FilmeId = 14, GeneroId = 8 },
                new GeneroFilme() { FilmeId = 15, GeneroId = 8 }, new GeneroFilme() { FilmeId = 15, GeneroId = 4 }, new GeneroFilme() { FilmeId = 15, GeneroId = 17 },
                new GeneroFilme() { FilmeId = 16, GeneroId = 17 }, new GeneroFilme() { FilmeId = 16, GeneroId = 7 }, new GeneroFilme() { FilmeId = 16, GeneroId = 10 },
                new GeneroFilme() { FilmeId = 17, GeneroId = 8 }, new GeneroFilme() { FilmeId = 17, GeneroId = 15 }, new GeneroFilme() { FilmeId = 17, GeneroId = 7 },
                new GeneroFilme() { FilmeId = 18, GeneroId = 16 }, new GeneroFilme() { FilmeId = 18, GeneroId = 3 }, new GeneroFilme() { FilmeId = 18, GeneroId = 9 },
                new GeneroFilme() { FilmeId = 19, GeneroId = 3 }, new GeneroFilme() { FilmeId = 19, GeneroId = 10 }, new GeneroFilme() { FilmeId = 19, GeneroId = 1 },
                new GeneroFilme() { FilmeId = 20, GeneroId = 11 }, new GeneroFilme() { FilmeId = 20, GeneroId = 10 }, new GeneroFilme() { FilmeId = 20, GeneroId = 13 },
                new GeneroFilme() { FilmeId = 21, GeneroId = 7 }, new GeneroFilme() { FilmeId = 21, GeneroId = 12 }, new GeneroFilme() { FilmeId = 21, GeneroId = 13 },
                new GeneroFilme() { FilmeId = 22, GeneroId = 11 }, new GeneroFilme() { FilmeId = 22, GeneroId = 13 }, new GeneroFilme() { FilmeId = 22, GeneroId = 3 },
                new GeneroFilme() { FilmeId = 23, GeneroId = 10 }, new GeneroFilme() { FilmeId = 23, GeneroId = 6 }, new GeneroFilme() { FilmeId = 23, GeneroId = 8 },
                new GeneroFilme() { FilmeId = 24, GeneroId = 10 }, new GeneroFilme() { FilmeId = 24, GeneroId = 11 }, new GeneroFilme() { FilmeId = 24, GeneroId = 3 },
                new GeneroFilme() { FilmeId = 25, GeneroId = 9 }, new GeneroFilme() { FilmeId = 25, GeneroId = 5 }, new GeneroFilme() { FilmeId = 25, GeneroId = 2 },
                new GeneroFilme() { FilmeId = 26, GeneroId = 17 }, new GeneroFilme() { FilmeId = 26, GeneroId = 6 }, new GeneroFilme() { FilmeId = 26, GeneroId = 4 },
                new GeneroFilme() { FilmeId = 27, GeneroId = 15 }, new GeneroFilme() { FilmeId = 27, GeneroId = 3 }, new GeneroFilme() { FilmeId = 27, GeneroId = 17 },
                new GeneroFilme() { FilmeId = 28, GeneroId = 14 }, new GeneroFilme() { FilmeId = 28, GeneroId = 2 }, new GeneroFilme() { FilmeId = 28, GeneroId = 16 },
                new GeneroFilme() { FilmeId = 29, GeneroId = 1 }, new GeneroFilme() { FilmeId = 29, GeneroId = 3 }, new GeneroFilme() { FilmeId = 29, GeneroId = 17 },
                new GeneroFilme() { FilmeId = 30, GeneroId = 11 }, new GeneroFilme() { FilmeId = 30, GeneroId = 5 }, new GeneroFilme() { FilmeId = 30, GeneroId = 6 },
                new GeneroFilme() { FilmeId = 31, GeneroId = 9 }, new GeneroFilme() { FilmeId = 31, GeneroId = 16 }, new GeneroFilme() { FilmeId = 31, GeneroId = 13 },
                new GeneroFilme() { FilmeId = 32, GeneroId = 12 }, new GeneroFilme() { FilmeId = 32, GeneroId = 2 }, new GeneroFilme() { FilmeId = 32, GeneroId = 13 },
                new GeneroFilme() { FilmeId = 33, GeneroId = 9 }, new GeneroFilme() { FilmeId = 33, GeneroId = 7 }, new GeneroFilme() { FilmeId = 33, GeneroId = 4 },
                new GeneroFilme() { FilmeId = 34, GeneroId = 4 }, new GeneroFilme() { FilmeId = 34, GeneroId = 14 }, new GeneroFilme() { FilmeId = 34, GeneroId = 17 },
                new GeneroFilme() { FilmeId = 35, GeneroId = 6 }, new GeneroFilme() { FilmeId = 35, GeneroId = 13 }, new GeneroFilme() { FilmeId = 35, GeneroId = 11 },
                new GeneroFilme() { FilmeId = 36, GeneroId = 12 }, new GeneroFilme() { FilmeId = 36, GeneroId = 7 }, new GeneroFilme() { FilmeId = 36, GeneroId = 3 },
                new GeneroFilme() { FilmeId = 37, GeneroId = 4 }, new GeneroFilme() { FilmeId = 37, GeneroId = 14 }, new GeneroFilme() { FilmeId = 37, GeneroId = 8 },
                new GeneroFilme() { FilmeId = 38, GeneroId = 13 }, new GeneroFilme() { FilmeId = 38, GeneroId = 10 }, new GeneroFilme() { FilmeId = 38, GeneroId = 11 },
                new GeneroFilme() { FilmeId = 39, GeneroId = 14 }, new GeneroFilme() { FilmeId = 39, GeneroId = 11 }, new GeneroFilme() { FilmeId = 39, GeneroId = 10 },
                new GeneroFilme() { FilmeId = 40, GeneroId = 3 }, new GeneroFilme() { FilmeId = 40, GeneroId = 17 }, new GeneroFilme() { FilmeId = 40, GeneroId = 2 },
                new GeneroFilme() { FilmeId = 41, GeneroId = 3 }, new GeneroFilme() { FilmeId = 41, GeneroId = 1 }, new GeneroFilme() { FilmeId = 41, GeneroId = 13 },
                new GeneroFilme() { FilmeId = 42, GeneroId = 8 }, new GeneroFilme() { FilmeId = 42, GeneroId = 14 }, new GeneroFilme() { FilmeId = 42, GeneroId = 4 },
                new GeneroFilme() { FilmeId = 43, GeneroId = 7 }, new GeneroFilme() { FilmeId = 43, GeneroId = 8 }, new GeneroFilme() { FilmeId = 43, GeneroId = 17 },
                new GeneroFilme() { FilmeId = 44, GeneroId = 1 }, new GeneroFilme() { FilmeId = 44, GeneroId = 11 }, new GeneroFilme() { FilmeId = 44, GeneroId = 7 },
                new GeneroFilme() { FilmeId = 45, GeneroId = 14 }, new GeneroFilme() { FilmeId = 45, GeneroId = 4 }, new GeneroFilme() { FilmeId = 45, GeneroId = 5 },
                new GeneroFilme() { FilmeId = 46, GeneroId = 9 }, new GeneroFilme() { FilmeId = 46, GeneroId = 6 }, new GeneroFilme() { FilmeId = 46, GeneroId = 4 },
                new GeneroFilme() { FilmeId = 47, GeneroId = 2 }, new GeneroFilme() { FilmeId = 47, GeneroId = 13 }, new GeneroFilme() { FilmeId = 47, GeneroId = 6 },
                new GeneroFilme() { FilmeId = 48, GeneroId = 16 }, new GeneroFilme() { FilmeId = 48, GeneroId = 9 }, new GeneroFilme() { FilmeId = 48, GeneroId = 3 },
                new GeneroFilme() { FilmeId = 49, GeneroId = 12 }, new GeneroFilme() { FilmeId = 49, GeneroId = 13 }, new GeneroFilme() { FilmeId = 49, GeneroId = 2 },
                new GeneroFilme() { FilmeId = 50, GeneroId = 14 }, new GeneroFilme() { FilmeId = 50, GeneroId = 3 }, new GeneroFilme() { FilmeId = 50, GeneroId = 15 });
        }

        public DbSet<Usuario> Users { get; set; }
        public DbSet<Perfil> Perfis { get; set; }
        public DbSet<Artista> Artistas { get; set; }
        public DbSet<Diretor> Diretores { get; set; }
        public DbSet<Genero> Generos { get; set; }
        public DbSet<AvaliacaoFilme> AvaliacaoFilmes { get; set; }
        public DbSet<ArtistaFilme> ArtistaFilmes {get; set;}
        public DbSet<DiretorFilme> DiretorFilmes {get; set;}
        public DbSet<GeneroFilme> GeneroFilmes{get; set;}
        public DbSet<Filme> Filmes { get; set; }
    }
}
