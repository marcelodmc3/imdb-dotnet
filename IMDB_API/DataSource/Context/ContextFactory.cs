﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DataSource.Context
{
    public class ContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var dbContextBuilder = new DbContextOptionsBuilder();
            return new DataContext(dbContextBuilder.Options);
        }
    }
}
