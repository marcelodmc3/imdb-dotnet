﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace DataSource.Entities
{
    public class Usuario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nome do usuário é obrigatório.")]
        [MinLength(5, ErrorMessage = "Nome do usário precisa ter no mínimo 5 caracteres.")]
        [MaxLength(200, ErrorMessage = "Nome do usário precisa ter no máximo 200 caracteres.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Login é obrigatório.")]
        [MinLength(5, ErrorMessage = "Login precisa ter no mínimo 5.")]
        [MaxLength(50, ErrorMessage = "Login precisa no máximo 50 caracteres.")]
        public string Login { get; set; }

        [Required]
        [MinLength(88)]
        [MaxLength(88)]
        [JsonIgnore]
        public string Hash { get; set; }

        [Required]
        [MinLength(44)]
        [MaxLength(44)]
        [JsonIgnore]
        public string Salt { get; set; }

        [Required(ErrorMessage = "Id do perfil é obrigatório.")]
        public int PerfilId { get; set; }

        public virtual Perfil Perfil { get; set; }

        [Required]
        [JsonIgnore]
        public bool Ativo { get; set; }
    }
}
