﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataSource.Entities
{
    public class Filme
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nome do filme é obrigatório.")]
        [MaxLength(200, ErrorMessage = "Nome do filme precisa ter no máximo 200 caracteres.")]
        public string Nome { get; set; }
        [Required]
        public int Ano { get; set; }
        public virtual IEnumerable<AvaliacaoFilme> Avaliacoes { get; set; }
        public virtual IEnumerable<ArtistaFilme> Artistas { get; set; }
        public virtual IEnumerable<GeneroFilme> Generos { get; set; }
        public virtual IEnumerable<DiretorFilme> Diretores { get; set; }
    }
}
