﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DataSource.Entities
{
    public class Perfil
    {
        [Key]
        [JsonIgnore]
        public int Id { get; set; }

        [Required(ErrorMessage = "Codigo do perfil é obrigatório.")]
        [MaxLength(50, ErrorMessage = "Código não pode possuir mais do que 50 caracteres.")]
        public string Codigo { get; set; }

        [Required(ErrorMessage = "Nome do perfil é obrigatório.")]
        [MaxLength(50, ErrorMessage = "Nome não pode possuir mais do que 50 caracteres.")]
        public string Nome { get; set; }

        [Required]
        [JsonIgnore]
        public bool Ativo { get; set; }
    }
}
