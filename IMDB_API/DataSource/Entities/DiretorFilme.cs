﻿using System.ComponentModel.DataAnnotations;

namespace DataSource.Entities
{
    public class DiretorFilme
    {
        [Required]
        public int FilmeId { get; set; }

        [Required]
        public int DiretorId { get; set; }

        public virtual Diretor Diretor { get; set; }
    }
}
