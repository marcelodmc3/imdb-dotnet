﻿using System.ComponentModel.DataAnnotations;

namespace DataSource.Entities
{
    public class GeneroFilme
    {
        [Required]
        public int FilmeId { get; set; }

        [Required]
        public int GeneroId { get; set; }

        public virtual Genero Genero { get; set; }
    }
}
