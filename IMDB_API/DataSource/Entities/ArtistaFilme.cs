﻿using System.ComponentModel.DataAnnotations;

namespace DataSource.Entities
{
    public class ArtistaFilme
    {
        [Required]
        public int FilmeId { get; set; }

        [Required]
        public int ArtistaId { get; set; }

        public virtual Artista Artista { get; set; }
    }
}
