﻿using System.ComponentModel.DataAnnotations;

namespace DataSource.Entities
{
    public class Genero
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nome do gênero é obrigatório.")]
        [MinLength(5, ErrorMessage = "Nome do gênero precisa ter no mínimo 5 caracteres.")]
        [MaxLength(200, ErrorMessage = "Nome do gênero precisa ter no máximo 200 caracteres.")]
        public string Nome { get; set; }
    }
}
