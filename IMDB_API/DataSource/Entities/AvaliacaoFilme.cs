﻿using System.ComponentModel.DataAnnotations;

namespace DataSource.Entities
{
    public class AvaliacaoFilme
    {
        [Required]
        public int UsuarioId { get; set; }

        [Required]
        public int FilmeId { get; set; }

        [Required]
        public int Avaliacao { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}
