﻿using Application.Services;
using Application.Utils;
using Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    [Route("api/v1/[controller]")]
    public class FilmeController : ControllerBase
    {
        private readonly FilmeService _filmeService;
        private readonly AtorService _atorService;
        private readonly DiretorService _diretorService;
        private readonly GeneroService _generoService;

        public FilmeController(
            FilmeService filmeService,
            AtorService atorService,
            DiretorService diretorService,
            GeneroService generoService)
        {
            _filmeService = filmeService;
            _atorService = atorService;
            _diretorService = diretorService;
            _generoService = generoService;
        }

        /// <summary>
        /// Cria um novo filme
        /// </summary>
        /// <param name="novoFilmeModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = PerfilUsuario.ADMINISTRADOR)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Criar([FromBody] NovoFilmeModel novoFilmeModel)
        {
            var validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            if (validation.Count == 0)
            {
                var dados = await _filmeService.CriarNovoFilme(novoFilmeModel);

                return Created($"api/v1/filme/{dados.Id}", dados);
            }

            return BadRequest(validation);
        }

        /// <summary>
        /// Da uma nota a um filme
        /// </summary>
        /// <param name="id">Id do Filme</param>
        /// <param name="nota">Nota do Filme</param>
        /// <returns></returns>
        [HttpPost("avaliar/{id}")]
        [Authorize(Roles = PerfilUsuario.USUARIO)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Avaliar(int id, [FromBody] int nota)
        {
            var validation = await _filmeService.AvaliarFilme(id, nota, User.Identity.Name.ToLower());

            if (validation.Count == 0)
            {
                return Created($"api/v1/filme/votar/{id}", nota);
            }

            return BadRequest(validation);
        }

        /// <summary>
        /// Busca as informações detalhadas de um filme
        /// </summary>
        /// <param name="id">Id do filme</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Obter(int id)
        {
            var filme = await _filmeService.Obter(id);
            if(filme != null)
            {
                return Ok(filme);
            }

            return NotFound();
        }

        /// <summary>
        /// Busca os filmes da plataforma de acordo com os parâmetros de paginação e filtro
        /// </summary>
        /// <param name="filtroFilmeModel"></param>
        /// <returns></returns>
        [HttpPost("listar")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Listar([FromBody] FiltroFilmeModel filtroFilmeModel)
        {
            var dados = await _filmeService.Filtrar(filtroFilmeModel);

            return Ok(dados);
        }

        /// <summary>
        /// Busca listagem de atores
        /// </summary>
        /// <param name="nome">Parâmetro de busca: Nome do ator/atora</param>
        /// <returns></returns>
        [HttpGet("buscar/atores")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> BuscarAtores([FromQuery] string nome)
        {
            var dados = await _atorService.Buscar(nome);
            if(dados != null && dados.Count > 0)
            {
                return Ok(dados);
            }

            return NotFound();
        }

        /// <summary>
        /// Busca listagem de gêneros
        /// </summary>
        /// <param name="nome">Parâmetro de busca: Nome do gênero de filme</param>
        /// <returns></returns>
        [HttpGet("buscar/generos")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> BuscarGeneros([FromQuery] string nome)
        {
            var dados = await _generoService.Buscar(nome);
            if (dados != null && dados.Count > 0)
            {
                return Ok(dados);
            }

            return NotFound();
        }

        /// <summary>
        /// Busca listagem de diretores
        /// </summary>
        /// <param name="nome">Parâmetro de busca: Nome do diretor</param>
        /// <returns></returns>
        [HttpGet("buscar/diretores")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> BuscarDiretores([FromQuery]string nome)
        {
            var dados = await _diretorService.Buscar(nome);
            if (dados != null && dados.Count > 0)
            {
                return Ok(dados);
            }

            return NotFound();
        }
    }
}
