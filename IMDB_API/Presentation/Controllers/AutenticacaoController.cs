﻿using Application.Services;
using Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    /// <summary>
    /// Serviços para autenticação de usuário
    /// </summary>
    [Route("api/v1/[controller]")]
    public class AutenticacaoController : ControllerBase
    {
        private readonly UsuarioService _usuarioService;

        public AutenticacaoController(UsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        /// <summary>
        /// Faz a autenticação de um usuário
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Login([FromBody] LoginModel login)
        {
            var dados = await _usuarioService.Login(login);
            if (dados != null)
            {
                return Ok(dados);
            }

            return NotFound("Usuário ou senha inválidos.");
        }
    }
}
