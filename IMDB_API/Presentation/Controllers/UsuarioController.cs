﻿using Application.Services;
using Application.Utils;
using Application.ViewModels;
using DataSource.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    [Route("api/v1/[controller]")]
    public class UsuarioController : ControllerBase
    {
        private readonly UsuarioService _usuarioService;


        public UsuarioController(UsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        /// <summary>
        /// Busca os usuários ativos na plataforma de acordo com os parâmetros de paginação
        /// </summary>
        /// <param name="filtroUsuarioModel">Parâmetros para a buscar/filtrar usuário</param>
        /// <returns>Listagem de usuário ativos na plataforma</returns>
        [HttpPost("listar")]
        [Authorize(Roles = PerfilUsuario.ADMINISTRADOR)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<Usuario>>> Listar([FromBody] FiltroUsuarioModel filtroUsuarioModel)
        {
            var dados = await _usuarioService.Listar(filtroUsuarioModel);

            return Ok(dados);
        }

        /// <summary>
        /// Cria um novo usuário
        /// </summary>
        /// <param name="usuarioModel">Dados do novo usuário</param>
        /// <returns></returns>
        [HttpPost("criar")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CriarUsuario([FromBody] NovoUsuarioModel usuarioModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _usuarioService.CadastrarUsuario(usuarioModel, User.Identity.Name != null ? User.Identity.Name.ToLower() : null);
                    if (result != null)
                    {
                        return Created("api/v1/usuario/", result);
                    }

                    return BadRequest($"Usuário '{usuarioModel.Login}' já existe ou o perfil '{usuarioModel.CodigoPerfil}' não é válido.");
                }
                else
                {
                    return BadRequest(ModelState);
                }

            }
            catch (UnauthorizedAccessException)
            {
                return BadRequest($"Somente Administradores podem criar outros usuários Administradores.");
            }
        }

        /// <summary>
        /// Desativa um usuário
        /// </summary>
        /// <param name="id">Id do usuário a ser desativado</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DesativarUsuario(int id)
        {
            try
            {
                var result = await _usuarioService.DesativarUsuario(id, User.Identity.Name.ToLower());
                if (result != null)
                {
                    return Ok();
                }

                return NotFound();
            }
            catch (UnauthorizedAccessException)
            {
                return BadRequest($"Somente Administradores podem alterar outros usuários.");
            }
        }

        /// <summary>
        /// Ativa um usuário
        /// </summary>
        /// <param name="id">Id do usuário a ser ativado</param>
        /// <returns></returns>
        [HttpPatch("ativar/{id}")]
        [Authorize(Roles = PerfilUsuario.ADMINISTRADOR)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AtivarUsuario(int id)
        {
            try
            {
                var result = await _usuarioService.AtivarUsuario(id, User.Identity.Name.ToLower());
                if (result != null)
                {
                    return Ok();
                }

                return NotFound();
            }
            catch (UnauthorizedAccessException)
            {
                return BadRequest($"Somente administradores podem alterar outros usuários.");
            }
        }

        /// <summary>
        /// Altera os dados de um usuário
        /// </summary>
        /// <param name="id">Id do usuário a ser alterado</param>
        /// <param name="alterarUsuarioModel">Novos dados dos usuário</param>
        /// <returns></returns>
        [HttpPatch("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AlterarUsuario(int id, [FromBody] AlterarUsuarioModel alterarUsuarioModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _usuarioService.AlterarUsuario(id, alterarUsuarioModel, User.Identity.Name.ToLower());
                    if (result != null)
                    {
                        return Ok();
                    }

                    return NotFound();
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (UnauthorizedAccessException uae)
            {
                return BadRequest(uae.Message);
            }
        }
    }
}
