﻿using Application.Services;
using DataSource.Context;
using Microsoft.Extensions.DependencyInjection;

namespace ApplicationTest
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // Banco de dados
            services.AddDbContext<DataContext>();
            services.AddScoped<DataContext, DataContext>();

            // Serviços
            services.AddScoped<UsuarioService, UsuarioService>();
            services.AddScoped<FilmeService, FilmeService>();
            services.AddScoped<AtorService, AtorService>();
            services.AddScoped<GeneroService, GeneroService>();
            services.AddScoped<DiretorService, DiretorService>();
        }
    }
}
