using Application.Services;
using Application.Utils;
using Application.ViewModels;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Xunit;

namespace ApplicationTest.Tests
{
    public class UsuarioTest
    {
        private UsuarioService _usuarioService;

        public UsuarioTest(UsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        [Fact]
        public async Task LoginUsuarioValido()
        {
            var login = new LoginModel()
            {
                Login = "usuarioadmin", 
                Senha = "SenhaSecreta"
            };

            var result = await _usuarioService.Login(login);

            Assert.NotNull(result);
        }

        [Fact]
        public async Task LoginUsuarioInvalido()
        {
            var login = new LoginModel()
            {
                Login = "asdfqwerty123",
                Senha = "SenhaSecreta"
            };

            var result = await _usuarioService.Login(login);

            Assert.Null(result);
        }

        [Fact]
        public async Task LoginSenhaInvalida()
        {
            var login = new LoginModel()
            {
                Login = "usuarioadmin",
                Senha = "asdfqwerty123"
            };

            var result = await _usuarioService.Login(login);

            Assert.Null(result);
        }

        [Fact]
        public async Task CadastroUsuarioAdministradorLogado()
        {
            string userdata = $"CadastroAdministradorLogado-{DateTime.Now.Ticks}";

            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = userdata,
                Login = userdata,
                Senha = userdata,
                CodigoPerfil = PerfilUsuario.ADMINISTRADOR
            };

            await _usuarioService.CadastrarUsuario(usuarioModel, "usuarioadmin");
            
            var login = new LoginModel()
            {
                Login = userdata,
                Senha = userdata
            };

            var result = await _usuarioService.Login(login);

            Assert.NotNull(result);
        }

        [Fact]
        public async Task CadastroUsuarioAdministradorAnomimo()
        {
            string userdata = $"CadastroAdministradorAnomimo-{DateTime.Now.Ticks}";

            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = userdata,
                Login = userdata,
                Senha = userdata,
                CodigoPerfil = PerfilUsuario.ADMINISTRADOR
            };


            await Assert.ThrowsAsync<UnauthorizedAccessException>(async () =>
            {
                await _usuarioService.CadastrarUsuario(usuarioModel, null);
            });
        }

        [Fact]
        public async Task CadastroUsuarioComum()
        {
            string userdata = $"CadastroUsuarioComum-{DateTime.Now.Ticks}";

            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = userdata,
                Login = userdata,
                Senha = userdata,
                CodigoPerfil = PerfilUsuario.USUARIO
            };

            await _usuarioService.CadastrarUsuario(usuarioModel, null);

            var login = new LoginModel()
            {
                Login = userdata,
                Senha = userdata
            };

            var result = await _usuarioService.Login(login);
            Assert.NotNull(result);
        }

        [Fact]
        public async Task CadastroCamposInvalidos()
        {
            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = "",
                Login = "   ",
                Senha = "    ",
                CodigoPerfil = "  "
            };

            await Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _usuarioService.CadastrarUsuario(usuarioModel, null);
            });
        }

        [Fact]
        public async Task CadastroUsuarioExistente()
        {
            string userdata = $"CadastroUsuarioExistente-{DateTime.Now.Ticks}";

            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = userdata,
                Login = userdata,
                Senha = userdata,
                CodigoPerfil = PerfilUsuario.USUARIO
            };

            await _usuarioService.CadastrarUsuario(usuarioModel, null);

            var result = await _usuarioService.CadastrarUsuario(usuarioModel, null);

            Assert.Null(result);
        }

        [Fact]
        public async Task ExclusaoLogicaProprioUsuario()
        {
            string userdata = $"ExclusaoLogicaProrio-{DateTime.Now.Ticks}";

            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = userdata,
                Login = userdata,
                Senha = userdata,
                CodigoPerfil = PerfilUsuario.USUARIO
            };

            await _usuarioService.CadastrarUsuario(usuarioModel, null);

            var login = new LoginModel()
            {
                Login = userdata,
                Senha = userdata
            };

            var result = await _usuarioService.Login(login);

            Assert.NotNull(result);

            await _usuarioService.DesativarUsuario(result.IdUsuario, userdata);

            result = await _usuarioService.Login(login);

            Assert.Null(result);
        }

        [Fact]
        public async Task ExclusaoLogicaUsuario()
        {
            string userdata = $"ExclusaoLogicaOutro-{DateTime.Now.Ticks}";

            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = userdata,
                Login = userdata,
                Senha = userdata,
                CodigoPerfil = PerfilUsuario.USUARIO
            };

            await _usuarioService.CadastrarUsuario(usuarioModel, null);

            var login = new LoginModel()
            {
                Login = userdata,
                Senha = userdata
            };

            var result = await _usuarioService.Login(login);

            Assert.NotNull(result);

            await Assert.ThrowsAsync<UnauthorizedAccessException>(async () =>
            {
                await _usuarioService.DesativarUsuario(result.IdUsuario, "usuariocomum");
            });
        }

        [Fact]
        public async Task ExclusaoLogicaAdministrador()
        {
            string userdata = $"ExclusaoLogicaAdministrador-{DateTime.Now.Ticks}";

            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = userdata,
                Login = userdata,
                Senha = userdata,
                CodigoPerfil = PerfilUsuario.USUARIO
            };

            await _usuarioService.CadastrarUsuario(usuarioModel, null);

            var login = new LoginModel()
            {
                Login = userdata,
                Senha = userdata
            };

            var result = await _usuarioService.Login(login);

            Assert.NotNull(result);

            await _usuarioService.DesativarUsuario(result.IdUsuario, "usuarioadmin");

            result = await _usuarioService.Login(login);

            Assert.Null(result);
        }

        [Fact]
        public async Task AlterarUsuarioComum()
        {
            string userdata = $"AlterarUsuarioComum-{DateTime.Now.Ticks}";

            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = userdata,
                Login = userdata,
                Senha = userdata,
                CodigoPerfil = PerfilUsuario.USUARIO
            };

            var user = await _usuarioService.CadastrarUsuario(usuarioModel, null);

            AlterarUsuarioModel alterarUsuarioModel = new AlterarUsuarioModel()
            {
                NovoCodigoPerfil = PerfilUsuario.USUARIO,
                ConfirmacaoSenha = userdata
            };

            userdata = $"AlterarUsuarioComumNovo-{DateTime.Now.Ticks}";

            alterarUsuarioModel.NovaSenha = userdata;
            alterarUsuarioModel.NovoNome = userdata;

            await _usuarioService.AlterarUsuario(user.Id, alterarUsuarioModel, user.Login);

            Assert.NotNull(await _usuarioService.Login(new LoginModel() { Login = user.Login, Senha = userdata }));
        }

        [Fact]
        public async Task AlterarUsuarioNaoAutorizado()
        {
            string userdata = $"AlterarNaoAltorizado-{DateTime.Now.Ticks}";

            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = userdata,
                Login = userdata,
                Senha = userdata,
                CodigoPerfil = PerfilUsuario.USUARIO
            };

            var user = await _usuarioService.CadastrarUsuario(usuarioModel, null);

            AlterarUsuarioModel alterarUsuarioModel = new AlterarUsuarioModel()
            {
                NovoCodigoPerfil = PerfilUsuario.ADMINISTRADOR,
                ConfirmacaoSenha = userdata
            };

            userdata = $"AlterarNaoAltorizadoNovo-{DateTime.Now.Ticks}";

            alterarUsuarioModel.NovaSenha = userdata;
            alterarUsuarioModel.NovoNome = userdata;

            await Assert.ThrowsAsync<UnauthorizedAccessException>(async () =>
            {
                await _usuarioService.AlterarUsuario(user.Id, alterarUsuarioModel, user.Login);
            });
        }

        [Fact]
        public async Task AlterarUsuarioDiferente()
        {
            string userdata = $"AlterarDiferente-{DateTime.Now.Ticks}";

            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = userdata,
                Login = userdata,
                Senha = userdata,
                CodigoPerfil = PerfilUsuario.USUARIO
            };

            var user = await _usuarioService.CadastrarUsuario(usuarioModel, null);

            AlterarUsuarioModel alterarUsuarioModel = new AlterarUsuarioModel()
            {
                NovoCodigoPerfil = PerfilUsuario.USUARIO,
                ConfirmacaoSenha = userdata
            };

            userdata = $"AlterarDiferenteNovo-{DateTime.Now.Ticks}";

            alterarUsuarioModel.NovaSenha = userdata;
            alterarUsuarioModel.NovoNome = userdata;

            await Assert.ThrowsAsync<UnauthorizedAccessException>(async () =>
            {
                await _usuarioService.AlterarUsuario(user.Id, alterarUsuarioModel, "usuariocomum");
            });
        }

        [Fact]
        public async Task AlterarUsuarioAdministrador()
        {
            string userdata = $"AlterarAdministrador-{DateTime.Now.Ticks}";

            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = userdata,
                Login = userdata,
                Senha = userdata,
                CodigoPerfil = PerfilUsuario.USUARIO
            };

            var user = await _usuarioService.CadastrarUsuario(usuarioModel, null);

            AlterarUsuarioModel alterarUsuarioModel = new AlterarUsuarioModel()
            {
                NovoCodigoPerfil = PerfilUsuario.ADMINISTRADOR,
                ConfirmacaoSenha = "SenhaSecreta"
            };

            userdata = $"AlterarAdministradorNovo-{DateTime.Now.Ticks}";

            alterarUsuarioModel.NovaSenha = userdata;
            alterarUsuarioModel.NovoNome = userdata;

            await _usuarioService.AlterarUsuario(user.Id, alterarUsuarioModel, "usuarioadmin");

            Assert.NotNull(await _usuarioService.Login(new LoginModel() { Login = user.Login, Senha = userdata }));
        }

        [Fact]
        public async Task AlterarUsuarioVazio()
        {
            string userdata = $"AlterarAdministrador-{DateTime.Now.Ticks}";

            NovoUsuarioModel usuarioModel = new NovoUsuarioModel()
            {
                Nome = userdata,
                Login = userdata,
                Senha = userdata,
                CodigoPerfil = PerfilUsuario.USUARIO
            };

            var user = await _usuarioService.CadastrarUsuario(usuarioModel, null);

            AlterarUsuarioModel alterarUsuarioModel = new AlterarUsuarioModel() { NovoNome = "   " };

            await Assert.ThrowsAsync<ValidationException>(async () =>
            {
                await _usuarioService.AlterarUsuario(user.Id, alterarUsuarioModel, "usuarioadmin");
            });
        }
    }
}
