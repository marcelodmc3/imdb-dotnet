﻿using Application.Services;
using Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ApplicationTest.Tests
{
    public class FilmeTest
    {
        private readonly FilmeService _filmeService;

        public FilmeTest(FilmeService filmeService)
        {
            _filmeService = filmeService;
        }

        [Fact]
        public async Task CriarFilmeNomeInvalido()
        {
            NovoFilmeModel novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 2020,
                ArtistasIds = new List<int>() { 1 },
                DiretoresIds = new List<int>() { 1 },
                GenerosIds = new List<int>() { 1 },
                Nome = "      "
            };

            var validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);

            novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 2020,
                ArtistasIds = new List<int>() { 1 },
                DiretoresIds = new List<int>() { 1 },
                GenerosIds = new List<int>() { 1 },
                Nome = null
            };

            validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);
        }

        [Fact]
        public async Task CriarFilmeAnoInvalido()
        {
            NovoFilmeModel novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 0,
                ArtistasIds = new List<int>() { 1 },
                DiretoresIds = new List<int>() { 1 },
                GenerosIds = new List<int>() { 1 },
                Nome = $"CriarFilmeAnoInvalido-{DateTime.Now.Ticks}"
            };

            var validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);

            novoFilmeModel = new NovoFilmeModel()
            {
                Ano = -555,
                ArtistasIds = new List<int>() { 1 },
                DiretoresIds = new List<int>() { 1 },
                GenerosIds = new List<int>() { 1 },
                Nome = $"CriarFilmeAnoInvalido-{DateTime.Now.Ticks}"
            };

            validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);
        }

        [Fact]
        public async Task CriarFilmeArtistasInvalidos()
        {
            NovoFilmeModel novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 2020,
                ArtistasIds = new List<int>(),
                DiretoresIds = new List<int>() { 1 },
                GenerosIds = new List<int>() { 1 },
                Nome = $"CriarFilmeArtistasInvalidos-{DateTime.Now.Ticks}"
            };

            var validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);

            novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 2020,
                ArtistasIds = new List<int>() { -5 },
                DiretoresIds = new List<int>() { 1 },
                GenerosIds = new List<int>() { 1 },
                Nome = $"CriarFilmeArtistasInvalidos-{DateTime.Now.Ticks}"
            };

            validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);

            novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 2020,
                ArtistasIds = null,
                DiretoresIds = new List<int>() { 1 },
                GenerosIds = new List<int>() { 1 },
                Nome = $"CriarFilmeArtistasInvalidos-{DateTime.Now.Ticks}"
            };

            validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);
        }

        [Fact]
        public async Task CriarFilmeGenerosInvalidos()
        {
            NovoFilmeModel novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 2020,
                ArtistasIds = new List<int>() { 1 },
                DiretoresIds = new List<int>() { 1 },
                GenerosIds = new List<int>(),
                Nome = $"CriarFilmeGenerosInvalidos-{DateTime.Now.Ticks}"
            };

            var validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);

            novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 2020,
                ArtistasIds = new List<int>() { 1 },
                DiretoresIds = new List<int>() { 1 },
                GenerosIds = new List<int>() { -5 },
                Nome = $"CriarFilmeGenerosInvalidos-{DateTime.Now.Ticks}"
            };

            validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);

            novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 2020,
                ArtistasIds = new List<int>() { 1 },
                DiretoresIds = new List<int>() { 1 },
                GenerosIds = null,
                Nome = $"CriarFilmeGenerosInvalidos-{DateTime.Now.Ticks}"
            };

            validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);
        }

        [Fact]
        public async Task CriarFilmeDiretoresInvalidos()
        {
            NovoFilmeModel novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 2020,
                ArtistasIds = new List<int>() { 1 },
                DiretoresIds = new List<int>(),
                GenerosIds = new List<int>() { 1 },
                Nome = $"CriarFilmeDiretoresInvalidos-{DateTime.Now.Ticks}"
            };

            var validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);

            novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 2020,
                ArtistasIds = new List<int>() { 1 },
                DiretoresIds = new List<int>() { -5 },
                GenerosIds = new List<int>() { 1 },
                Nome = $"CriarFilmeDiretoresInvalidos-{DateTime.Now.Ticks}"
            };

            validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);

            novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 2020,
                ArtistasIds = new List<int>() { 1 },
                DiretoresIds = null,
                GenerosIds = new List<int>() { 1 },
                Nome = $"CriarFilmeDiretoresInvalidos-{DateTime.Now.Ticks}"
            };

            validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);
        }

        [Fact]
        public async Task CriarFilmeNomeRepetido()
        {
            NovoFilmeModel novoFilmeModel = new NovoFilmeModel()
            {
                Ano = 2020,
                ArtistasIds = new List<int>() { 1 },
                DiretoresIds = new List<int>() { 1 },
                GenerosIds = new List<int>() { 1 },
                Nome = $"CriarFilmeNomeRepetido-{DateTime.Now.Ticks}"
            };

            var validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);
            Assert.Empty(validation);

            var filme = await _filmeService.CriarNovoFilme(novoFilmeModel);

            Assert.NotNull(filme);

            validation = await _filmeService.ValidarNovoFilme(novoFilmeModel);

            Assert.NotEmpty(validation);
        }

        [Fact]
        public async Task AvaliarFilmeInvalido()
        {
            var validadion = await _filmeService.AvaliarFilme(2, 4, "usuarioadmin");
            Assert.NotEmpty(validadion);

            validadion = await _filmeService.AvaliarFilme(2, 5, "usuariocomum");
            Assert.NotEmpty(validadion);

            validadion = await _filmeService.AvaliarFilme(2, -5, "usuariocomum");
            Assert.NotEmpty(validadion);

            validadion = await _filmeService.AvaliarFilme(-1, 3, "usuariocomum");
            Assert.NotEmpty(validadion);
        }

        [Fact]
        public async Task AvaliarFilmeValido()
        {
            var validadion = await _filmeService.AvaliarFilme(2, 4, "usuariocomum");
            Assert.Empty(validadion);
        }
    }
}
