﻿using System.Linq;

namespace Application.Utils
{
    public static class IQueryableHelper
    {
        public static IQueryable<TModel> Paginar<TModel>(this IQueryable<TModel> query, int pageNumber = 0, int pageSize = 0) where TModel : class
        {
            return pageSize > 0 && pageNumber > 0 ? query.Skip((pageNumber - 1) * pageSize).Take(pageSize) : query;
        }
    }
}
