﻿using Application.ViewModels;
using DataSource.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Application.Utils
{
    public static class TokenGenerator
    {
        private const string PRIVATE_KEY = "tOwF9HXmFpd2JxQTcZd9wuRHu3SWhioGwphAy4qLNZ2KiJGXQg88DotuBOI5YkWbnoCShc4nD2LYb9V32D3N/w==";

        public static TokenViewModel GerarToken(Usuario usuario)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            byte[] chaveBytes = Convert.FromBase64String(PRIVATE_KEY);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, usuario.Login.ToLower()),
                    new Claim(ClaimTypes.Role, usuario.Perfil.Codigo)
                }),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(chaveBytes), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return new TokenViewModel()
            {
                Token = tokenHandler.WriteToken(token),
                Tipo = JwtBearerDefaults.AuthenticationScheme,
                NomeUsuario = usuario.Nome,
                IdUsuario = usuario.Id,
                NomePerfil = usuario.Perfil.Nome
            };
        }
    }
}
