﻿using DataSource.Entities;
using System.Collections.Generic;

namespace Application.ViewModels
{
    public class ListagemUsuariosModel
    {
        public int Total { get; set; }

        public int Selecionados { get { return Usuarios != null ? Usuarios.Count : 0; } }

        public int Pagina { get; set; }

        public List<Usuario> Usuarios { get; set; }
    }
}
