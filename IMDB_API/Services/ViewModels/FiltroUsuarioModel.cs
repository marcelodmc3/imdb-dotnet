﻿namespace Application.ViewModels
{
    public class FiltroUsuarioModel
    {
        public bool Paginar { get; set; }
        public int NumeroPagina { get; set; }
        public int TamanhoPagina { get; set; }
    }
}
