﻿namespace Application.ViewModels
{
    public class IdNomeModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
