﻿using System.ComponentModel.DataAnnotations;

namespace Application.ViewModels
{
    public class NovoUsuarioModel
    {
        [Required(ErrorMessage = "Nome do usuário é obrigatório.")]
        [MinLength(5, ErrorMessage = "Nome do usário precisa ter no mínimo 5 caracteres.")]
        [MaxLength(200, ErrorMessage = "Nome do usário precisa ter no máximo 200 caracteres.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Login é obrigatório.")]
        [MinLength(5, ErrorMessage = "Login precisa ter no mínimo 5 caracteres.")]
        [MaxLength(50, ErrorMessage = "Login precisa no máximo 50 caracteres.")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Senha é obrigatória.")]
        [MinLength(8, ErrorMessage = "Senha precisa ter no mínimo 8 caracteres.")]
        [MaxLength(50, ErrorMessage = "Senha precisa ter no máximo 50 caracteres.")]
        public string Senha { get; set; }

        [Required(ErrorMessage = "Codigo do perfil é obrigatório.")]
        [MaxLength(50, ErrorMessage = "Código não pode possuir mais do que 50 caracteres.")]
        public string CodigoPerfil { get; set; }
    }
}
