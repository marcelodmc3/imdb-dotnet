﻿using System.Collections.Generic;

namespace Application.ViewModels
{
    public class ListagemFilmesModel
    {
        public int Total { get; set; }

        public int Selecionados { get { return Filmes != null ? Filmes.Count : 0; } }

        public int Pagina { get; set; }

        public List<FilmeViewModel> Filmes { get; set; }
    }
}
