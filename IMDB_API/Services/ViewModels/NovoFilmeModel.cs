﻿using System.Collections.Generic;

namespace Application.ViewModels
{
    public class NovoFilmeModel
    {
        public List<int> ArtistasIds { get; set; }
        public List<int> GenerosIds { get; set; }
        public List<int> DiretoresIds { get; set; }
        public string Nome { get; set; }
        public int Ano { get; set; }
    }
}
