﻿using System.Collections.Generic;

namespace Application.ViewModels
{
    public class FiltroFilmeModel
    {
        public bool Paginar { get; set; }
        public int NumeroPagina { get; set; }
        public int TamanhoPagina { get; set; }
        public List<int> DiretorIds { get; set; }
        public List<int> GeneroIds { get; set; }
        public List<int> AtoresIds { get; set; }
        public string Nome { get; set; }
        public bool OrdenarPorAvaliacao { get; set; }
    }
}
