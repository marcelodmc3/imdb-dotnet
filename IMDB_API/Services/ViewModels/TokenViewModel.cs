﻿namespace Application.ViewModels
{
    public class TokenViewModel
    {
        public string Token { get; set; }
        public string Tipo { get; set; }
        public int IdUsuario { get; set; }
        public string NomeUsuario { get; set; }
        public string NomePerfil { get; set; }
    }
}
