﻿using System.Collections.Generic;

namespace Application.ViewModels
{
    public class FilmeViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int Ano { get; set; }
        public double AvaliacaoMedia { get; set; }
        public List<IdNomeModel> Atores { get; set; }
        public List<IdNomeModel> Diretores { get; set; }
        public List<IdNomeModel> Generos { get; set; }
    }
}
