﻿using Application.ViewModels;
using DataSource.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services
{
    public class AtorService
    {
        private readonly DataContext _dataContext;

        public AtorService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<List<IdNomeModel>> Buscar(string nome)
        {
            var query = _dataContext.Artistas.Where(a => a.Id > 0);

            if(!string.IsNullOrWhiteSpace(nome))
            {
                string filtroFormatado = "%" + nome.ToLower() + "%";
                query = query.Where(a => EF.Functions.Like(a.Nome.ToLower(), filtroFormatado));
            }

            return await query
                .AsNoTracking()
                .Select(a => new IdNomeModel() { Id = a.Id, Nome = a.Nome })
                .OrderBy(a => a.Nome)
                .ToListAsync();
        }
    }
}
