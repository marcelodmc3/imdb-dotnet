﻿using Application.Utils;
using Application.ViewModels;
using DataSource.Context;
using DataSource.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services
{
    public class UsuarioService
    {
        private readonly DataContext _dataContext;

        public UsuarioService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<ListagemUsuariosModel> Listar(FiltroUsuarioModel filtroUsuarioModel)
        {
            ListagemUsuariosModel lista = new ListagemUsuariosModel();

            var query = _dataContext.Users
                    .Include(u => u.Perfil)
                    .Where(u => u.Ativo)
                    .Where(u => u.Perfil.Codigo.Equals(PerfilUsuario.USUARIO));

            if (!filtroUsuarioModel.Paginar)
            {
                lista.Total = await query.CountAsync();
                lista.Usuarios = await query
                    .OrderBy(u => u.Nome)
                    .AsNoTracking()
                    .ToListAsync();
            }

            else
            {
                int pagina = filtroUsuarioModel.NumeroPagina >= 1 ? filtroUsuarioModel.NumeroPagina : 1;
                int tamanhopagina = filtroUsuarioModel.TamanhoPagina >= 1 ? filtroUsuarioModel.TamanhoPagina : 10;

                lista.Pagina = pagina;

                lista.Total = await query.CountAsync();
                lista.Usuarios = await query
                    .OrderBy(u => u.Nome)
                    .Paginar(pagina, tamanhopagina)
                    .AsNoTracking()
                    .ToListAsync();
            }

            return lista;
        }

        public async Task<TokenViewModel> Login(LoginModel login)
        {
            var usuario = await _dataContext
                .Users
                .Include(u => u.Perfil)
                .AsNoTracking()
                .Where(u => u.Perfil.Ativo)
                .Where(u => u.Ativo)
                .Where(u => u.Login.Equals(login.Login.ToLower()))
                .FirstOrDefaultAsync();

            if (usuario != null)
            {
                bool valid = PasswordHasher.IsValid(login.Senha, usuario.Hash, usuario.Salt);

                if (valid)
                {
                    return TokenGenerator.GerarToken(usuario);
                }
            }

            return null;
        }

        public async Task<Usuario> CadastrarUsuario(NovoUsuarioModel novoUsuarioModel, string loggeduser)
        {
            if (novoUsuarioModel != null)
            {
                novoUsuarioModel = new NovoUsuarioModel()
                {
                    Nome = novoUsuarioModel.Nome?.Trim(),
                    Login = novoUsuarioModel.Login?.Trim(),
                    Senha = novoUsuarioModel.Senha?.Trim(),
                    CodigoPerfil = novoUsuarioModel.CodigoPerfil?.Trim()
                };

                var context = new ValidationContext(novoUsuarioModel, serviceProvider: null, items: null);
                var validationResults = new List<ValidationResult>();

                bool isValid = Validator.TryValidateObject(novoUsuarioModel, context, validationResults, true);

                if (!isValid)
                    throw new ValidationException("Campos inválidos");

                else
                {
                    Usuario usuariologado = null; 

                    if(!string.IsNullOrWhiteSpace(loggeduser))
                    {
                        usuariologado = await _dataContext.Users
                            .Include(u => u.Perfil)
                            .AsNoTracking()
                            .FirstOrDefaultAsync(u => u.Login.Equals(loggeduser));
                    }

                    if((usuariologado == null || usuariologado.Perfil.Codigo.Equals(PerfilUsuario.USUARIO)) && !novoUsuarioModel.CodigoPerfil.Equals(PerfilUsuario.USUARIO))
                        throw new UnauthorizedAccessException();

                    await _dataContext.Users
                        .Include(u => u.Perfil)
                        .AsNoTracking()
                        .FirstOrDefaultAsync(u => u.Login.Equals(loggeduser));

                    var jaexiste = await _dataContext.Users
                        .AnyAsync(u => u.Login.Equals(novoUsuarioModel.Login.ToLower()));

                    if (!jaexiste)
                    {
                        var perfil = await _dataContext.Perfis
                            .Where(p => p.Codigo.Equals(novoUsuarioModel.CodigoPerfil))
                            .AsNoTracking()
                            .FirstOrDefaultAsync();

                        if (perfil != null)
                        {
                            var hash = PasswordHasher.GerarHash(novoUsuarioModel.Senha);

                            Usuario usuario = new Usuario()
                            {
                                PerfilId = perfil.Id,
                                Nome = novoUsuarioModel.Nome,
                                Login = novoUsuarioModel.Login.ToLower(),
                                Hash = hash[0],
                                Salt = hash[1],
                                Ativo = true
                            };

                            await _dataContext.Users.AddAsync(usuario);
                            await _dataContext.SaveChangesAsync();

                            usuario.Perfil = perfil;
                            return usuario;
                        }
                    }
                }
            }
            else throw new ValidationException($"{nameof(novoUsuarioModel)} esta nulo.");

            return null;
        }

        public async Task<Usuario> DesativarUsuario(int id, string loggeduser)
        {
            var usuariologado = await _dataContext.Users
                .Include(u => u.Perfil)
                .AsNoTracking()
                .FirstOrDefaultAsync(u => u.Login.Equals(loggeduser));

            var usuario = await _dataContext.Users
                .FirstOrDefaultAsync(u => u.Id.Equals(id));

            if (usuario != null && usuariologado != null)
            {
                if (usuariologado.Perfil.Codigo.Equals(PerfilUsuario.USUARIO) && !usuariologado.Id.Equals(usuario.Id))
                    throw new UnauthorizedAccessException();

                usuario.Ativo = false;
                _dataContext.Users.Update(usuario);
                await _dataContext.SaveChangesAsync();

                return usuario;
            }
            return null;
        }

        public async Task<Usuario> AtivarUsuario(int id, string loggeduser)
        {
            var usuariologado = await _dataContext.Users
                .Include(u => u.Perfil)
                .AsNoTracking()
                .FirstOrDefaultAsync(u => u.Login.Equals(loggeduser));

            var usuario = await _dataContext.Users
                .FirstOrDefaultAsync(u => u.Id.Equals(id));

            if (usuario != null)
            {
                if (usuariologado.Perfil.Codigo.Equals(PerfilUsuario.USUARIO) && !usuariologado.Id.Equals(usuario.Id))
                    throw new UnauthorizedAccessException();

                usuario.Ativo = true;
                _dataContext.Users.Update(usuario);
                await _dataContext.SaveChangesAsync();
            }
            return null;
        }

        public async Task<Usuario> AlterarUsuario(int id, AlterarUsuarioModel alterarUsuarioModel, string loggeduser)
        {
            if (alterarUsuarioModel != null)
            {
                alterarUsuarioModel = new AlterarUsuarioModel()
                {
                    ConfirmacaoSenha = alterarUsuarioModel.ConfirmacaoSenha?.Trim(),
                    NovaSenha = alterarUsuarioModel.NovaSenha?.Trim(),
                    NovoNome = alterarUsuarioModel.NovoNome?.Trim(),
                    NovoCodigoPerfil = alterarUsuarioModel.NovoCodigoPerfil?.Trim()
                };

                var context = new ValidationContext(alterarUsuarioModel, serviceProvider: null, items: null);
                var validationResults = new List<ValidationResult>();

                bool isValid = Validator.TryValidateObject(alterarUsuarioModel, context, validationResults, true);

                if (!isValid)
                    throw new ValidationException("Campos inválidos");

                var usuariologado = await _dataContext.Users
                .Include(u => u.Perfil)
                .AsNoTracking()
                .FirstOrDefaultAsync(u => u.Login.Equals(loggeduser));

                var usuario = await _dataContext.Users
                    .FirstOrDefaultAsync(u => u.Id.Equals(id));

                if (usuario != null && usuariologado != null)
                {
                    bool valid = PasswordHasher.IsValid(alterarUsuarioModel.ConfirmacaoSenha, usuariologado.Hash, usuariologado.Salt);

                    if (valid)
                    {
                        if (usuariologado.Perfil.Codigo.Equals(PerfilUsuario.USUARIO) && !usuariologado.Id.Equals(usuario.Id))
                            throw new UnauthorizedAccessException("Somente Administradores podem alterar outros usuários.");

                        if (usuariologado.Perfil.Codigo.Equals(PerfilUsuario.USUARIO) && !alterarUsuarioModel.NovoCodigoPerfil.Equals(PerfilUsuario.USUARIO))
                            throw new UnauthorizedAccessException("Somente Administradores podem alterar o perfil de usuários.");

                        var perfil = await _dataContext.Perfis
                                .Where(p => p.Codigo.Equals(alterarUsuarioModel.NovoCodigoPerfil))
                                .AsNoTracking()
                                .FirstOrDefaultAsync();

                        var hash = PasswordHasher.GerarHash(alterarUsuarioModel.NovaSenha);

                        usuario.PerfilId = perfil.Id;
                        usuario.Hash = hash[0];
                        usuario.Salt = hash[1];
                        usuario.Nome = alterarUsuarioModel.NovoNome;

                        _dataContext.Users.Update(usuario);
                        await _dataContext.SaveChangesAsync();

                        return usuario;
                    }
                }

                else return null;

                throw new UnauthorizedAccessException("Confirmação de senha inválida.");
            }
            else throw new ValidationException($"{nameof(alterarUsuarioModel)} esta nulo.");
        }
    }
}
