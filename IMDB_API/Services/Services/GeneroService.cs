﻿using Application.ViewModels;
using DataSource.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services
{
    public class GeneroService
    {
        private readonly DataContext _dataContext;

        public GeneroService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<List<IdNomeModel>> Buscar(string nome)
        {
            var query = _dataContext.Generos.Where(a => a.Id > 0);

            if (!string.IsNullOrWhiteSpace(nome))
            {
                string filtroFormatado = "%" + nome.ToLower() + "%";
                query = query.Where(g => EF.Functions.Like(g.Nome.ToLower(), filtroFormatado));
            }

            return await query
                .AsNoTracking()
                .Select(g => new IdNomeModel() { Id = g.Id, Nome = g.Nome })
                .OrderBy(g => g.Nome)
                .ToListAsync();
        }
    }
}
