﻿using Application.Utils;
using Application.ViewModels;
using DataSource.Context;
using DataSource.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FilmeService
    {
        private readonly DataContext _dataContext;

        public FilmeService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<List<string>> ValidarNovoFilme(NovoFilmeModel novoFilmeModel)
        {
            List<string> validation = new List<string>();

            novoFilmeModel.Nome = novoFilmeModel.Nome?.Trim();

            if (novoFilmeModel.ArtistasIds == null || novoFilmeModel.ArtistasIds.Count == 0)
            {
                validation.Add("O filme precisa conter pelo menos 1 (um) ator.");
            }
            else
            {
                List<int> invalidArtist = new List<int>();

                foreach (int id in novoFilmeModel.ArtistasIds)
                {
                    int count = await _dataContext.Artistas.Where(a => a.Id.Equals(id)).CountAsync();
                    if (count == 0) invalidArtist.Add(id);
                }

                if (invalidArtist.Count > 0)
                {
                    validation.Add($"Os atores com os Ids '{string.Join(", ", invalidArtist)}' não foram encontrados.");
                }
            }

            if (novoFilmeModel.GenerosIds == null || novoFilmeModel.GenerosIds.Count == 0)
            {
                validation.Add("O filme precisa conter pelo menos 1 (um) gêneros.");
            }
            else
            {
                List<int> invalidGeneros = new List<int>();

                foreach (int id in novoFilmeModel.GenerosIds)
                {
                    int count = await _dataContext.Generos.Where(a => a.Id.Equals(id)).CountAsync();
                    if (count == 0) invalidGeneros.Add(id);
                }

                if (invalidGeneros.Count > 0)
                {
                    validation.Add($"Os gêneros com os Ids '{string.Join(", ", invalidGeneros)}' não foram encontrados.");
                }
            }

            if (novoFilmeModel.DiretoresIds == null || novoFilmeModel.DiretoresIds.Count == 0)
            {
                validation.Add("O filme precisa conter pelo menos 1 (um) diretor.");
            }
            else
            {
                List<int> invalidDiretores = new List<int>();

                foreach (int id in novoFilmeModel.DiretoresIds)
                {
                    int count = await _dataContext.Diretores.Where(a => a.Id.Equals(id)).CountAsync();
                    if (count == 0) invalidDiretores.Add(id);
                }

                if (invalidDiretores.Count > 0)
                {
                    validation.Add($"Os diretores com os Ids '{string.Join(", ", invalidDiretores)}' não foram encontrados.");
                }
            }

            if (string.IsNullOrEmpty(novoFilmeModel.Nome))
                validation.Add("O nome do filme é obrigatório.");
            else
            {
                if (novoFilmeModel.Nome.Length > 200)
                    validation.Add("Nome do filme precisa ter no máximo 200 caracteres.");
                else
                {
                    bool repetido = await _dataContext.Filmes
                        .Where(u => u.Nome.ToLower().Equals(novoFilmeModel.Nome.ToLower()))
                        .AnyAsync();

                    if (repetido) validation.Add($"Já existe um filme com o nome '{novoFilmeModel.Nome}'.");
                }
            }

            if (novoFilmeModel.Ano <= 0)
                validation.Add("O ano de lançamento do filme tem que ser maior do que 0 (zero).");

            return validation;
        }

        public async Task<Filme> CriarNovoFilme(NovoFilmeModel novoFilmeModel)
        {
            using (var trasactioncontext = _dataContext.Database.BeginTransaction())
            {
                try
                {
                    Filme filme = new Filme()
                    {
                        Nome = novoFilmeModel.Nome,
                        Ano = novoFilmeModel.Ano
                    };

                    await _dataContext.Filmes.AddAsync(filme);
                    await _dataContext.SaveChangesAsync();

                    await _dataContext.ArtistaFilmes.AddRangeAsync(novoFilmeModel.ArtistasIds.Select(
                            a => new ArtistaFilme()
                            {
                                FilmeId = filme.Id,
                                ArtistaId = a
                            }));

                    await _dataContext.GeneroFilmes.AddRangeAsync(novoFilmeModel.GenerosIds.Select(
                            g => new GeneroFilme()
                            {
                                FilmeId = filme.Id,
                                GeneroId = g
                            }));

                    await _dataContext.DiretorFilmes.AddRangeAsync(novoFilmeModel.DiretoresIds.Select(
                            d => new DiretorFilme()
                            {
                                FilmeId = filme.Id,
                                DiretorId = d
                            }));

                    await _dataContext.SaveChangesAsync();
                    trasactioncontext.Commit();

                    return filme;
                }
                catch
                {
                    trasactioncontext.Rollback();
                    throw;
                }
            }
        }

        public async Task<List<string>> AvaliarFilme(int id, int nota, string loggeduser)
        {
            List<string> validation = new List<string>();

            if (nota >= 0 && nota <= 4)
            {
                var usuariologado = await _dataContext.Users
                                .Include(u => u.Perfil)
                                .Where(u => u.Ativo)
                                .Where(u => u.Perfil.Codigo.Equals(PerfilUsuario.USUARIO))
                                .AsNoTracking()
                                .FirstOrDefaultAsync(u => u.Login.Equals(loggeduser));

                if (usuariologado != null)
                {
                    var filme = await _dataContext.Filmes
                        .AsNoTracking()
                        .FirstOrDefaultAsync(f => f.Id.Equals(id));

                    if (filme != null)
                    {
                        AvaliacaoFilme avaliacaoFilme = await _dataContext.AvaliacaoFilmes
                            .AsNoTracking()
                            .FirstOrDefaultAsync(af => af.FilmeId.Equals(id) && af.UsuarioId.Equals(usuariologado.Id));

                        if (avaliacaoFilme != null)
                        {
                            avaliacaoFilme.Avaliacao = nota;
                            _dataContext.AvaliacaoFilmes.Update(avaliacaoFilme);
                        }
                        else
                        {
                            avaliacaoFilme = new AvaliacaoFilme()
                            {
                                FilmeId = id,
                                UsuarioId = usuariologado.Id,
                                Avaliacao = nota
                            };

                            await _dataContext.AvaliacaoFilmes.AddRangeAsync(avaliacaoFilme);
                        }

                        await _dataContext.SaveChangesAsync();

                    }
                    else validation.Add($"Não foi possível encontrar o filme com o Id '{id}'");
                }
                else validation.Add($"O usuário '{loggeduser}' não tem permissão para avaliar um filme.");

            }
            else validation.Add("A nota do filme tem que ser entre 0 (zero) e 4 (quatro).");

            return validation;
        }

        public async Task<FilmeViewModel> Obter(int id)
        {
            FilmeViewModel filme = await _dataContext.Filmes
                .Include(f => f.Avaliacoes)
                .Include(f => f.Artistas)
                    .ThenInclude(a => a.Artista)
                .Include(f => f.Generos)
                    .ThenInclude(g => g.Genero)
                .Include(f => f.Diretores)
                    .ThenInclude(d => d.Diretor)
                .Where(f => f.Id.Equals(id))
                .AsNoTracking()
                .Select(f => new FilmeViewModel()
                {
                    Id = f.Id,
                    Nome = f.Nome,
                    Ano = f.Ano,
                    AvaliacaoMedia = f.Avaliacoes.Select(a => a.Avaliacao).Count() > 0 ? 
                        f.Avaliacoes.Select(a => a.Avaliacao).Sum() / (double)f.Avaliacoes.Select(a => a.Avaliacao).Count() : 0.0,
                    Atores = f.Artistas.Select(a => new IdNomeModel() { Id = a.Artista.Id, Nome = a.Artista.Nome}).ToList(),
                    Generos = f.Generos.Select(a => new IdNomeModel() { Id = a.Genero.Id, Nome = a.Genero.Nome }).ToList(),
                    Diretores = f.Diretores.Select(a => new IdNomeModel() { Id = a.Diretor.Id, Nome = a.Diretor.Nome }).ToList()
                })
                .FirstOrDefaultAsync();

            return filme;
        }

        public async Task<ListagemFilmesModel> Filtrar(FiltroFilmeModel filtroFilmeModel)
        {
            ListagemFilmesModel listagemFilmesModel = new ListagemFilmesModel();

            var query = _dataContext.Filmes
                .Include(f => f.Avaliacoes)
                .Include(f => f.Artistas)
                    .ThenInclude(a => a.Artista)
                .Include(f => f.Generos)
                    .ThenInclude(g => g.Genero)
                .Include(f => f.Diretores)
                    .ThenInclude(d => d.Diretor)
                .Where(f => f.Id > 0);

            if (filtroFilmeModel.GeneroIds != null && filtroFilmeModel.GeneroIds.Count > 0)
            {
                query = query
                    .Where(f => f.Generos.Select(g => g.GeneroId).Any(g => filtroFilmeModel.GeneroIds.Contains(g)));
            }

            if (filtroFilmeModel.AtoresIds != null && filtroFilmeModel.AtoresIds.Count > 0)
            {
                query = query
                    .Where(f => f.Artistas.Select(a => a.ArtistaId).Any(a => filtroFilmeModel.AtoresIds.Contains(a)));
            }

            if (filtroFilmeModel.DiretorIds != null && filtroFilmeModel.DiretorIds.Count > 0)
            {
                query = query
                    .Where(f => f.Diretores.Select(d => d.DiretorId).Any(d => filtroFilmeModel.DiretorIds.Contains(d)));
            }

            if (!string.IsNullOrWhiteSpace(filtroFilmeModel.Nome))
            {
                string filtroFormatado = "%" + filtroFilmeModel.Nome.ToLower() + "%";
                query = query.Where(f => EF.Functions.Like(f.Nome.ToLower(), filtroFormatado));
            }

            if(filtroFilmeModel.OrdenarPorAvaliacao)
            {
                query = query.OrderByDescending(f => f.Avaliacoes.Select(a => a.Avaliacao).Count() > 0 ?
                        f.Avaliacoes.Select(a => a.Avaliacao).Sum() / (double)f.Avaliacoes.Select(a => a.Avaliacao).Count() : 0.0);
            }
            else
            {
                query = query.OrderBy(f => f.Nome);
            }

            int pagina = filtroFilmeModel.Paginar ? (filtroFilmeModel.NumeroPagina >= 1 ? filtroFilmeModel.NumeroPagina : 1) : 0;
            int tamanhopagina = filtroFilmeModel.Paginar ? (filtroFilmeModel.TamanhoPagina >= 1 ? filtroFilmeModel.TamanhoPagina : 10) : 0;

            listagemFilmesModel.Total = query.Count();

            query = query.Paginar(pagina, tamanhopagina);
            
            listagemFilmesModel.Pagina = pagina;
            listagemFilmesModel.Filmes = await query
            .AsNoTracking()
            .Select(f => new FilmeViewModel()
            {
                Id = f.Id,
                Nome = f.Nome,
                Ano = f.Ano,
                AvaliacaoMedia = f.Avaliacoes.Select(a => a.Avaliacao).Count() > 0 ?
                        f.Avaliacoes.Select(a => a.Avaliacao).Sum() / (double)f.Avaliacoes.Select(a => a.Avaliacao).Count() : 0.0,
                Atores = f.Artistas.Select(a => new IdNomeModel() { Id = a.Artista.Id, Nome = a.Artista.Nome }).ToList(),
                Generos = f.Generos.Select(a => new IdNomeModel() { Id = a.Genero.Id, Nome = a.Genero.Nome }).ToList(),
                Diretores = f.Diretores.Select(a => new IdNomeModel() { Id = a.Diretor.Id, Nome = a.Diretor.Nome }).ToList()
            }).ToListAsync();

            return listagemFilmesModel;
        }
    }
}
