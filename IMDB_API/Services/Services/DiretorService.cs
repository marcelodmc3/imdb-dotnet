﻿using Application.ViewModels;
using DataSource.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Services
{
    public class DiretorService
    {
        private readonly DataContext _dataContext;

        public DiretorService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<List<IdNomeModel>> Buscar(string nome)
        {
            var query = _dataContext.Diretores.Where(a => a.Id > 0);

            if (!string.IsNullOrWhiteSpace(nome))
            {
                string filtroFormatado = "%" + nome.ToLower() + "%";
                query = query.Where(d => EF.Functions.Like(d.Nome.ToLower(), filtroFormatado));
            }

            return await query
                .AsNoTracking()
                .Select(d => new IdNomeModel() { Id = d.Id, Nome = d.Nome })
                .OrderBy(d => d.Nome)
                .ToListAsync();
        }
    }
}
