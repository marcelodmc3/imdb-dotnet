﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.ServiceModels
{
    public class Perfil
    {
        public string Codigo { get; set; }

        public string Nome { get; set; }
    }
}
