﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.ServiceModels
{
    public class UsuarioLogadoModel
    {
        public string Token { get; set; }
        public string Tipo { get; set; }
        public int IdUsuario { get; set; }
        public string NomeUsuario { get; set; }
        public string NomePerfil { get; set; }
    }
}
