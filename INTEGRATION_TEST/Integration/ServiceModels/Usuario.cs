﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.ServiceModels
{
    public class Usuario
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Login { get; set; }

        public int PerfilId { get; set; }

        public Perfil Perfil { get; set; }

    }
}
