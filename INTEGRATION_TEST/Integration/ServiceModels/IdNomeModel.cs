﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.ServiceModels
{
    public class IdNomeModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
