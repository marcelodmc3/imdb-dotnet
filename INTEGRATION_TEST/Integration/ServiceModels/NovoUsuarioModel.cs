﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.ServiceModels
{
    public class NovoUsuarioModel
    {
        public string Nome { get; set; }

        public string Login { get; set; }

        public string Senha { get; set; }

        public string CodigoPerfil { get; set; }
    }
}
