﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.ServiceModels
{
    public class Filme
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public int Ano { get; set; }
    }
}
