﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Integration
{
    public class Watcher : BackgroundService
    {
        private readonly ILogger<Watcher> _logger;

        public Watcher(
            ILogger<Watcher> logger
            )
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while(true)
            {
                await Task.Delay(30000, stoppingToken);

                _logger.LogWarning("Se os testes ainda não foram finalizados, verifique o endereço do serviço em 'appsettings.json' ou se o mesmo está sendo executado.");
            }
        }
    }
}
