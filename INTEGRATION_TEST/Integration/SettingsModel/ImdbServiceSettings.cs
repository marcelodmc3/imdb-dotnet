﻿namespace Integration.SettingsModel
{
    public class ImdbServiceSettings
    {
        public string BaseUrl { get; set; }
        public AutenticacaoSettings Autenticacao { get; set; }
        public FilmeSettings Filme { get; set; }
        public UsuarioSettings Usuario { get; set; }
    }
}
