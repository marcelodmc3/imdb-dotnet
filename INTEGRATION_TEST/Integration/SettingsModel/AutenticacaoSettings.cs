﻿namespace Integration.SettingsModel
{
    public class AutenticacaoSettings
    {
        public string Autenticar { get; set; }
        public string AdminLogin { get; set; }
        public string UserLogin { get; set; }
        public string AdminSenha { get; set; }
        public string UserSenha { get; set; }
    }
}
