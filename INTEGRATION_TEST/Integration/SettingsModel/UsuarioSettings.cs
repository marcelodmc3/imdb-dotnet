﻿namespace Integration.SettingsModel
{
    public class UsuarioSettings
    {
        public string ListarUsuarios { get; set; }
        public string CriarUsuario { get; set; }
        public string DesativarUsuario { get; set; }
        public string AlterarUsuario { get; set; }
        public string AtivarUsuario { get; set; }
    }
}
