﻿namespace Integration.SettingsModel
{
    public class FilmeSettings
    {
        public string CadastrarFilme { get; set;}
        public string AvaliarFilme { get; set;}
        public string BuscarDadosFilme { get; set; }
        public string ListarFilmes { get; set; }
        public string BuscarAtores { get; set; }
        public string BuscarGeneros  { get; set; }
        public string BuscarDiretores { get; set; }
    }
}
