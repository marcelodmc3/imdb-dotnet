using Integration.ServiceModels;
using Integration.SettingsModel;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Integration
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly ImdbServiceSettings _config;

        public Worker(
            ILogger<Worker> logger,
            IConfiguration configuration
            )
        {
            _logger = logger;
            _config = configuration.GetSection("ImdbServiceSettings")
                .Get<ImdbServiceSettings>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            bool?[] testes = new bool?[10];

            try
            {
                UsuarioLogadoModel admin = null;
                UsuarioLogadoModel user = null;
                Usuario criado = null;

                List<IdNomeModel> diretores = null;
                List<IdNomeModel> generos = null;
                List<IdNomeModel> atores = null;

                Filme filme = null;
                
                using (HttpClient httpclient = new HttpClient())
                {
                    try
                    {
                        _logger.LogInformation("Logando com usu�rio Administrador...\n");
                        await Task.Delay(1000, stoppingToken);
                        httpclient.BaseAddress = new Uri(_config.BaseUrl);

                        var body = new StringContent(JsonConvert.SerializeObject(
                        new LoginModel()
                        {
                            Login = _config.Autenticacao.AdminLogin,
                            Senha = _config.Autenticacao.AdminSenha
                        }),
                        Encoding.UTF8, "application/json");

                        HttpResponseMessage resposta = await httpclient.PostAsync(_config.Autenticacao.Autenticar, body);
                        var dadosResposta = await resposta.Content.ReadAsStringAsync();

                        if (resposta.IsSuccessStatusCode)
                        {
                            admin = JsonConvert.DeserializeObject<UsuarioLogadoModel>(dadosResposta);
                            if (admin != null && !string.IsNullOrWhiteSpace(admin.Token))
                            {
                                _logger.LogInformation("Usuario logado: {s}\n", dadosResposta);
                                testes[0] = true;
                            }
                            else
                            {
                                _logger.LogError("Servi�o de login n�o retornou os dados do usu�rio logado conforme o esperado {s}\n", dadosResposta);
                                admin = null;
                                testes[0] = false;
                            }
                        }
                        else
                        {
                            _logger.LogError("Erro ao realizar o login: {s}\n", dadosResposta);
                            testes[0] = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError("Erro inesperado: {s}", ex.Message);
                        testes[0] = false;
                    }
                }

                using (HttpClient httpclient = new HttpClient())
                {
                    try
                    {
                        _logger.LogInformation("Logando com usu�rio...\n");
                        await Task.Delay(1000, stoppingToken);
                        httpclient.BaseAddress = new Uri(_config.BaseUrl);

                        var body = new StringContent(JsonConvert.SerializeObject(
                        new LoginModel()
                        {
                            Login = _config.Autenticacao.UserLogin,
                            Senha = _config.Autenticacao.UserSenha
                        }),
                        Encoding.UTF8, "application/json");

                        HttpResponseMessage resposta = await httpclient.PostAsync(_config.Autenticacao.Autenticar, body);
                        var dadosResposta = await resposta.Content.ReadAsStringAsync();

                        if (resposta.IsSuccessStatusCode)
                        {
                            user = JsonConvert.DeserializeObject<UsuarioLogadoModel>(dadosResposta);
                            if (user != null && !string.IsNullOrWhiteSpace(admin.Token))
                            {
                                _logger.LogInformation("Usuario logado: {s}\n", dadosResposta);
                                testes[1] = true;
                            }
                            else
                            {
                                _logger.LogError("Servi�o de login n�o retornou os dados do usu�rio logado conforme o esperado {s}\n", dadosResposta);
                                user = null;
                                testes[1] = false;
                            }
                        }
                        else
                        {
                            _logger.LogError("Erro ao realizar o login: {s}\n", dadosResposta);
                            testes[1] = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError("Erro inesperado: {s}", ex.Message);
                        testes[1] = false;
                    }
                }

                if (admin != null)
                {
                    using (HttpClient httpclient = new HttpClient())
                    {
                        try
                        {
                            _logger.LogInformation("Cadastrando usuario...\n");
                            await Task.Delay(1000, stoppingToken);
                            httpclient.BaseAddress = new Uri(_config.BaseUrl);

                            httpclient.DefaultRequestHeaders.Add("Authorization", $"{admin.Tipo} {admin.Token}");

                            string userdata = $"CadastroNovoUsuario-{DateTime.Now.Ticks}".ToLower();

                            var body = new StringContent(JsonConvert.SerializeObject(
                            new NovoUsuarioModel()
                            {
                                Nome = userdata,
                                Login = userdata,
                                Senha = userdata,
                                CodigoPerfil = "USUARIO"
                            }),
                            Encoding.UTF8, "application/json");

                            HttpResponseMessage resposta = await httpclient.PostAsync(_config.Usuario.CriarUsuario, body);
                            var dadosResposta = await resposta.Content.ReadAsStringAsync();

                            if (resposta.IsSuccessStatusCode)
                            {
                                criado = JsonConvert.DeserializeObject<Usuario>(dadosResposta);
                                if (criado != null && criado.Id > 0)
                                {
                                    _logger.LogInformation("Usuario cadastrado: {s}\n", dadosResposta);
                                    testes[2] = true;
                                }
                                else
                                {
                                    _logger.LogError("Servi�o de login n�o retornou os dados do usu�rio criado conforme o esperado {s}\n", dadosResposta);
                                    criado = null;
                                    testes[2] = false;
                                }
                            }
                            else
                            {
                                _logger.LogError("Erro ao realizar o cadastro: {s}\n", dadosResposta);
                                testes[2] = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError("Erro inesperado: {s}", ex.Message);
                            testes[2] = false;
                        }
                    }

                    if (criado != null)
                    {
                        using (HttpClient httpclient = new HttpClient())
                        {
                            try
                            {
                                _logger.LogInformation("Logando com usu�rio rec�m criado...\n");
                                await Task.Delay(1000, stoppingToken);
                                httpclient.BaseAddress = new Uri(_config.BaseUrl);

                                var body = new StringContent(JsonConvert.SerializeObject(
                                new LoginModel()
                                {
                                    Login = criado.Login,
                                    Senha = criado.Login
                                }),
                                Encoding.UTF8, "application/json");

                                HttpResponseMessage resposta = await httpclient.PostAsync(_config.Autenticacao.Autenticar, body);
                                var dadosResposta = await resposta.Content.ReadAsStringAsync();

                                if (resposta.IsSuccessStatusCode)
                                {
                                    var loggeduser = JsonConvert.DeserializeObject<UsuarioLogadoModel>(dadosResposta);
                                    if (loggeduser != null && !string.IsNullOrWhiteSpace(admin.Token))
                                    {
                                        _logger.LogInformation("Usuario logado: {s}\n", dadosResposta);
                                        testes[3] = true;
                                    }
                                    else
                                    {
                                        _logger.LogError("Servi�o de login n�o retornou os dados do usu�rio logado conforme o esperado {s}\n", dadosResposta);
                                        testes[3] = false;
                                    }
                                }
                                else
                                {
                                    _logger.LogError("Erro ao realizar o login: {s}\n", dadosResposta);
                                    testes[3] = false;
                                }

                            }
                            catch (Exception ex)
                            {
                                _logger.LogError("Erro inesperado: {s}", ex.Message);
                                testes[3] = false;
                            }
                        }
                    }

                    if (user != null)
                    {
                        using (HttpClient httpclient = new HttpClient())
                        {
                            try
                            {
                                _logger.LogInformation("Desativando usu�rio rec�m criado...\n");
                                await Task.Delay(1000, stoppingToken);
                                httpclient.BaseAddress = new Uri(_config.BaseUrl);

                                httpclient.DefaultRequestHeaders.Add("Authorization", $"{admin.Tipo} {admin.Token}");

                                HttpResponseMessage resposta = await httpclient.DeleteAsync(_config.Usuario.DesativarUsuario.Replace("{id}", criado.Id.ToString()));
                                var dadosResposta = await resposta.Content.ReadAsStringAsync();

                                if (resposta.IsSuccessStatusCode)
                                {
                                    _logger.LogInformation("Usuario desativado\n");
                                    testes[4] = true;
                                }
                                else
                                {
                                    _logger.LogError("Erro ao desativar o usu�rio: {s}\n", dadosResposta);
                                    testes[4] = false;
                                }

                            }
                            catch (Exception ex)
                            {
                                _logger.LogError("Erro inesperado: {s}", ex.Message);
                                testes[4] = false;
                            }
                        }
                    }

                    using (HttpClient httpclient = new HttpClient())
                    {
                        try
                        {
                            _logger.LogInformation("Listando diretores...\n");
                            await Task.Delay(1000, stoppingToken);
                            httpclient.BaseAddress = new Uri(_config.BaseUrl);

                            httpclient.DefaultRequestHeaders.Add("Authorization", $"{admin.Tipo} {admin.Token}");

                            HttpResponseMessage resposta = await httpclient.GetAsync(_config.Filme.BuscarDiretores);
                            var dadosResposta = await resposta.Content.ReadAsStringAsync();

                            if (resposta.IsSuccessStatusCode)
                            {
                                diretores = JsonConvert.DeserializeObject<List<IdNomeModel>>(dadosResposta);
                                if (diretores != null && diretores.Count > 0)
                                {
                                    _logger.LogInformation("Lista de diretoress: {s}\n", dadosResposta);
                                    testes[5] = true;
                                }
                                else
                                {
                                    _logger.LogError("Servi�o de listagem n�o retornou os dados dos diretores conforme o esperado: {s}\n", dadosResposta);
                                    criado = null;
                                    testes[5] = false;
                                    diretores = null;
                                }
                            }
                            else
                            {
                                _logger.LogError("Erro ao buscar a listagem de diretores: {s}\n", dadosResposta);
                                testes[5] = false;
                                diretores = null;
                            }

                        }
                        catch (Exception ex)
                        {
                            _logger.LogError("Erro inesperado: {s}", ex.Message);
                            testes[4] = false;
                        }
                    }

                    using (HttpClient httpclient = new HttpClient())
                    {
                        try
                        {
                            _logger.LogInformation("Listando atores...\n");
                            await Task.Delay(1000, stoppingToken);
                            httpclient.BaseAddress = new Uri(_config.BaseUrl);

                            httpclient.DefaultRequestHeaders.Add("Authorization", $"{admin.Tipo} {admin.Token}");

                            HttpResponseMessage resposta = await httpclient.GetAsync(_config.Filme.BuscarAtores);
                            var dadosResposta = await resposta.Content.ReadAsStringAsync();

                            if (resposta.IsSuccessStatusCode)
                            {
                                atores = JsonConvert.DeserializeObject<List<IdNomeModel>>(dadosResposta);
                                if (atores != null && atores.Count > 0)
                                {
                                    _logger.LogInformation("Lista de atores: {s}\n", dadosResposta);
                                    testes[6] = true;
                                }
                                else
                                {
                                    _logger.LogError("Servi�o de listagem n�o retornou os dados dos atores conforme o esperado: {s}\n", dadosResposta);
                                    criado = null;
                                    testes[6] = false;
                                    atores = null;
                                }
                            }
                            else
                            {
                                _logger.LogError("Erro ao buscar a listagem de atores: {s}\n", dadosResposta);
                                testes[6] = false;
                                atores = null;
                            }

                        }
                        catch (Exception ex)
                        {
                            _logger.LogError("Erro inesperado: {s}", ex.Message);
                            testes[6] = false;
                        }
                    }

                    using (HttpClient httpclient = new HttpClient())
                    {
                        try
                        {
                            _logger.LogInformation("Listando g�neros...\n");
                            await Task.Delay(1000, stoppingToken);
                            httpclient.BaseAddress = new Uri(_config.BaseUrl);

                            httpclient.DefaultRequestHeaders.Add("Authorization", $"{admin.Tipo} {admin.Token}");

                            HttpResponseMessage resposta = await httpclient.GetAsync(_config.Filme.BuscarAtores);
                            var dadosResposta = await resposta.Content.ReadAsStringAsync();

                            if (resposta.IsSuccessStatusCode)
                            {
                                generos = JsonConvert.DeserializeObject<List<IdNomeModel>>(dadosResposta);
                                if (atores != null && atores.Count > 0)
                                {
                                    _logger.LogInformation("Lista de g�neros: {s}\n", dadosResposta);
                                    testes[7] = true;
                                }
                                else
                                {
                                    _logger.LogError("Servi�o de listagem n�o retornou os dados dos g�neros conforme o esperado: {s}\n", dadosResposta);
                                    criado = null;
                                    testes[7] = false;
                                    generos = null;
                                }
                            }
                            else
                            {
                                _logger.LogError("Erro ao buscar a listagem de g�neros: {s}\n", dadosResposta);
                                testes[7] = false;
                                generos = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError("Erro inesperado: {s}", ex.Message);
                            testes[7] = false;
                        }
                    }

                    if (diretores != null && atores != null && generos != null)
                    {
                        using (HttpClient httpclient = new HttpClient())
                        {
                            try
                            {
                                _logger.LogInformation("Cadastrando filme...\n");
                                await Task.Delay(1000, stoppingToken);
                                httpclient.BaseAddress = new Uri(_config.BaseUrl);

                                httpclient.DefaultRequestHeaders.Add("Authorization", $"{admin.Tipo} {admin.Token}");

                                var body = new StringContent(JsonConvert.SerializeObject(
                                new NovoFilmeModel()
                                {
                                    Nome = $"CadastroNovoFilme-{DateTime.Now.Ticks}",
                                    Ano = 2020,
                                    ArtistasIds = new List<int>() { atores[0].Id },
                                    GenerosIds = new List<int>() { generos[0].Id },
                                    DiretoresIds = new List<int>() { diretores[0].Id },
                                }),
                                Encoding.UTF8, "application/json");

                                HttpResponseMessage resposta = await httpclient.PostAsync(_config.Filme.CadastrarFilme, body);
                                var dadosResposta = await resposta.Content.ReadAsStringAsync();

                                if (resposta.IsSuccessStatusCode)
                                {
                                    filme = JsonConvert.DeserializeObject<Filme>(dadosResposta);
                                    if (filme != null && filme.Id > 0)
                                    {
                                        _logger.LogInformation("Filme cadastrado: {s}\n", dadosResposta);
                                        testes[8] = true;
                                    }
                                    else
                                    {
                                        _logger.LogError("Servi�o de cadastro n�o retornou os dados do filme conforme o esperado {s}\n", dadosResposta);
                                        filme = null;
                                        testes[8] = false;
                                    }
                                }
                                else
                                {
                                    _logger.LogError("Erro ao realizar o cadastro: {s}\n", dadosResposta);
                                    testes[8] = false;
                                }
                            }
                            catch (Exception ex)
                            {
                                _logger.LogError("Erro inesperado: {s}", ex.Message);
                                testes[8] = false;
                            }
                        }

                        if (filme != null)
                        {
                            using (HttpClient httpclient = new HttpClient())
                            {
                                try
                                {
                                    _logger.LogInformation("Avaliando filme...\n");
                                    await Task.Delay(1000, stoppingToken);
                                    httpclient.BaseAddress = new Uri(_config.BaseUrl);

                                    httpclient.DefaultRequestHeaders.Add("Authorization", $"{user.Tipo} {user.Token}");

                                    var body = new StringContent(JsonConvert.SerializeObject(4),Encoding.UTF8, "application/json");

                                    HttpResponseMessage resposta = await httpclient.PostAsync(_config.Filme.AvaliarFilme.Replace("{id}", filme.Id.ToString()), body);
                                    var dadosResposta = await resposta.Content.ReadAsStringAsync();

                                    if (resposta.IsSuccessStatusCode)
                                    {
                                        int nota = JsonConvert.DeserializeObject<int>(dadosResposta);
                                        if (nota >=0 && nota <= 4)
                                        {
                                            _logger.LogInformation("Filme avaliado: {s}\n", dadosResposta);
                                            testes[9] = true;
                                        }
                                        else
                                        {
                                            _logger.LogError("Servi�o de avali��o n�o retornou a nota esperada {s}\n", dadosResposta);
                                            testes[9] = false;
                                        }
                                    }
                                    else
                                    {
                                        _logger.LogError("Erro ao realizar a avalia��o do filme: {s}\n", dadosResposta);
                                        testes[9] = false;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger.LogError("Erro inesperado: {s}", ex.Message);
                                    testes[9] = false;
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro inesperado: {s}", ex.Message);
            }
            finally
            {
                _logger.LogInformation("- - - - - - - - -\n");

                for (int i = 0; i < testes.Length; i++)
                {
                    int t = i + 1;

                    bool? teste = testes[i];
                    string tag = $"Teste {t.ToString("00")}";

                    if (teste.HasValue)
                    {
                        if (teste.Value)
                            _logger.LogInformation($"{tag} bem sucessido.\n");
                        else
                            _logger.LogError($"{tag} falhou.\n");
                    }
                    else
                    {
                        _logger.LogWarning($"{tag} n�o executado.\n");
                    }
                }

                _logger.LogInformation("- - - - - - - - -\n");
                _logger.LogInformation("Teste finalizado.\n");
            }
        }
    }
}
